<?php

namespace App\Repositories;

use App\Repositories\GatewayAbstract;
use Stripe\Charge;
use Stripe\Stripe as StripeLibClass;
use App\Repositories\Setting;

class Stripe extends GatewayAbstract
{

	public function __construct(Setting $setting)
	{
		$this->setting = $setting;
	}



	public function gatewayName()
	{
		return 'STRIPE';
	}



	/**
	*	Used to charge paymanet and return informations array
	*/
	public function charge($data = [])
	{
		StripeLibClass::setApiKey($this->setting->get('stripe_api_server_key'));
        
        $charge_token = $data['charge_token'];

        try {

          	$charge = Charge::create([
            	"amount" => $data['amount']*100,
            	"currency" => $data['auth_user']->currency,
            	"source" => $charge_token,
            	"description" => $data['description']
            ]);


          	return [
				'success'        => $charge->paid,
				'transaction_id' => $charge->balance_transaction,
				'status'         => $charge->status,
				'amount'         => $charge->amount/100,
				'extra'          => $charge->__toArray(true)
          	];



        } catch(\Exception $e) {

        	return [
				'success'        => false,
				'transaction_id' => 'X-X-X-X-X',
				'status'         => 'failed',
				'amount'         => $data['amount'],
				'extra'          => [
					'try_catch_error_message' => $e->getMessage()
				]
          	];
        }

 
	}





	/**
	*	Used to test stripe charge from admin panel and get whole response
	*/
	public function testCharge($data = [])
	{

		StripeLibClass::setApiKey($this->setting->get('stripe_api_server_key'));

		try {

			$charge = Charge::create([
	        	"amount" => $data['amount']*100,
	        	"currency" => $data['currency'],
	        	"source" => $data['charge_token'],
	        	"description" => $data['description']
	        ]);

			return $charge->__toArray(true);

		} catch(\Exception $e){
			return ['error_message' => $e->getMessage()];
		}


	}




}