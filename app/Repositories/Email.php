<?php

namespace App\Repositories;

use Mail;

class Email
{

	public function sendRegistrationEmail($user)
	{
		return $this->send('registration', 'Welcome to Digital Wallet', ['user' => $user]);
	}


	public function sendOtpEmail($user, $otpCode)
	{
		return $this->send(
			'otp', 
			'Digital Wallet Regitration otp', 
			['user' => $user, 'otp_code' => $otpCode]
		);
	}


	public function send($mTemplateName, $subject, $dataArray, &$error = "")
	{
		try {

			if(isset($dataArray['user']->email) 
				&& !is_null($dataArray['user']->email) 
				&& !empty($dataArray['user']->email)
			) {

				$user = $dataArray['user'];
				Mail::send('emails.'.$mTemplateName, 
					$dataArray, 
					function ($m) use ($user, $subject) {

		            $m->to($user->email, $user->first_name)->subject($subject);

		        });

				return true;
			}

			return false;
		} catch(\Exception $e) {
			$error = $e->getMessage();
			return false;
		}

	}


}