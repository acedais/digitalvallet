<?php

namespace App\Repositories;

abstract class GatewayAbstract
{

	protected static $gatewayClassMaps = [
		'stripe' => 'App\\Repositories\\Stripe',
		'paypal'  => 'App\\Repositories\\Paypal',
		'senangpay' => 'App\\Repositories\\senang'
	];

	abstract public function gatewayName();
	abstract public function charge($data = []);
	
	public static function instance($gName)
	{
		return app(self::$gatewayClassMaps[strtolower($gName)]);
	} 
}