<?php

namespace App\Repositories;


/**
* Setting class
* 
* This class is part of DigitalWallet product. 
* Used for storing admin panel settings like facebook, twilio api keys. as an key value
*
* @package Digital Wallet
* @author  provenlogic
*/


use App\Models\AccessToken;
use App\Models\User;


class Setting
{

	/**
	* returns filename settings.php for config func() config
	*/
	protected $settings_config_file_name = 'settings';


	/**
	* holds the whole settings
	*/
	protected $settings = [];


	/**
	* initializing $this->settings variable with all settings
	*/
	public function __construct()
	{
		$this->all();
	}




	/**
	* returns the absolute path for settings.php config file
	*/
	protected function settingConfigPath()
	{
		return config_path($this->settings_config_file_name.'.php');
	}

	
	/**
	* returns all the admin settings array as key value
	*/	
	public function all()
	{
		return $this->settings = config($this->settings_config_file_name);
	}



	/**
	* returns specific config by key
	*/
	public function get($key)
	{
		return config($this->settings_config_file_name.'.'.$key);
	}



	/**
	* set a specific setting 
	*
	* @param stirng $key settings key
	* @param stirng $value settings value
	*/
	public function set($key, $value)
	{
		$this->settings[$key] = $value;
		config([$this->settings_config_file_name.'.'.$key => $value]);
		$this->writeArrayToFile($this->settings, $this->settingConfigPath());
	}


	
	/**
	* takes all settings from config (not from file) and set $this->settings variable
	*
	*/
	protected function upSync()
	{
		$this->settings = config($this->settings_config_file_name);
	}



	/**
	* save array to file as php script
	*
	* @param array $array this must be array
	* @param string $file this must be absolute file path
	*/
	protected function writeArrayToFile($arry, $file) 
	{
        $string = var_export($arry, true);
        $string = "<?php return \n".$string . ";";
        file_put_contents($file, $string, LOCK_EX);
        return true;
     
    }




}