<?php

namespace App\Repositories;

use App\Repositories\Api;
use DB;
use Hash;
use App\Models\User as UserModel;
use App\Repositories\Otp;
use Validator;

class User
{

	public function __construct(UserModel $user, Otp $otp, Api $api)
	{
		$this->user  = $user;
		$this->otp = $otp;
		$this->api = $api;
	}


	public function findUserByMobile($countryCode, $mobile)
	{
		return $this->user->where('mobile_no', $mobile)
					->where('country_code', $countryCode)
					->first();
	}




	public function verifyOtpAndChangePassword($newPwd, $otpCode, $mNo, $cCode, &$resData)
	{

		$validator = Validator::make([
				'new_password' => $newPwd,
				'otp_code'     => $otpCode,
				'mobile_no'    => $mNo,
				'country_code' => $cCode
			], 
			[
				'otp_code'     => 'required|numeric',
				'new_password' => 'required|min:8|max:100',
				'country_code' => 'required',
				'mobile_no'    => 'required|numeric'
        	]
        );

        if($validator->fails()) {
        	$e = $validator->errors();
        	$msg = [];
            $e->has('new_password') ? $msg['new_password'] = $e->get('new_password')[0]: '';
            $e->has('otp_code') ? $msg['otp_code'] = $e->get('otp_code')[0]: '';
            $e->has('country_code') ? $msg['country_code'] = $e->get('country_code')[0]: '';
            $e->has('mobile_no') ? $msg['mobile_no'] = $e->get('mobile_no')[0]: '';
            $resData = $msg;
            return false;
        }


        $user = $this->findUserByMobile($cCode, $mNo);

        if(!$user) return false;

        $success = $this->otp->verifyOTP(
            'REGISTRATION', 
            $user->fullMobileno(), 
            $otpCode
        );

        if(!$success) {
        	$resData = ['otp_code' => 'Invalid otp code entered'];
        	return false;
        }

        $user->password = Hash::make($newPwd);
        $user->save();

        $this->api->removingAllAccesstokens($user->id);

        $resData = [
        	'access_token' => $this->api->createAndSaveAccessToken($user->id)->token
        ];

       	return true;

	}	












    public function getTotalUsersCount() 
    { 
        return $this->user->count(); 
    }



    public function getUsersCountByCreatedDate($dateSting) 
    {
            return $this->user
            ->where('created_at', 'like', $dateSting.'-%')
            ->count();  
    }




    public function getCountrySignUps() 
    {
        $groups = \DB::select('SELECT country_code, count(*) as count From users group by country_code');
        
        $countries = [];
        foreach($groups as $group) {

            if(($cName = $this->getCountryByPhoneCode($group->country_code)) == "" ) continue;

            $countries[$cName] = $group->count;
        }

        return $countries;
    }



    public function getCountryByPhoneCode($code)
    {
        $content = file_get_contents(config_path('country_codes.json'));
        $countryArray = json_decode($content, true)['countries'];
        $key = array_search($code, array_column($countryArray, 'code'));
        return $key ? $countryArray[$key]['name'] : "";
    }



    public function getUsers($perPage)
    {
        return $this->user->orderBy('created_at', 'desc')->paginate($perPage);
    }




    public function activateUsers($userIDs)
    {
        return $this->queryUserByids($userIDs)->update(['status' => 'ACTIAVATED']);
    }


    public function deActivateUsers($userIDs)
    {
        return $this->queryUserByids($userIDs)->update(['status' => 'DEACTIVATED_BY_ADMIN']);
    }



    protected function queryUserByids($userIDs)
    {
        return $this->user->whereIn('id', $userIDs);
    }



    public function deleteUsers($userIDs)
    {
        
        foreach($userIDs as $id) {

            //delete each user with db transaction
            try {

                DB::beginTransaction();

                $user = app('App\Models\User')->find($id);

                if(!$user) continue;

                app('App\Models\AccessToken')->where('entity_id', $user->id)->forceDelete();
                app('App\Models\FitbitUser')->where('user_id', $user->id)->forceDelete();
                app('App\Models\FitbitUserStepGoal')->where('user_id', $user->id)->forceDelete();
                app('App\Models\Order')->leftJoin('transactions', 'orders.trans_table_id', '=', 'transactions.id')->where('orders.user_id', $user->id)->orWhere('orders.other_user_id', $user->id)->forceDelete();
                app('App\Models\OtpToken')->where('mobile_no', $user->fullMobileno())->forceDelete();
                app('App\Models\SocialLogin')->where('user_id', $user->id)->forceDelete();
                
                $user->forceDelete();

                DB::commit();

            } catch(\Exception $e) {
                DB::rollback();
                \Log::info($e->getMessage());
            }



        }


    }


}