<?php

namespace App\Repositories;

use PayPal\Rest\ApiContext;
use App\Repositories\GatewayAbstract;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payment;
use App\Repositories\Setting;


class Paypal extends GatewayAbstract
{

	public function __construct(Setting $setting)
	{

		$this->setting = $setting;

		$this->apiContext = new ApiContext(
		    new OAuthTokenCredential(
		    	$this->setting->get('paypal_client_id'), 
		    	$this->setting->get('paypal_secret_key')
		    )
		);

		if($this->setting->get('paypal_live_mode') == 'true') {
			$this->apiContext->setConfig(['mode' => 'live']);
		}
		
	}

	public function gatewayName()
	{
		return 'PAYPAL';
	}


	public function charge($data = [])
	{


		try {
			$payment = Payment::get($data['payment_id'], $this->apiContext);

			$transactions = $payment->getTransactions();

			if($payment->getState() === 'approved') {

				$related_resources = $transactions[0]->getRelatedResources();
				$sale = $related_resources[0]->getSale();
				$sale_id = $sale->getId();

				return [
					'success'        => true,
					'transaction_id' => $sale_id,
					'status'         => 'approved',
					'amount'         => $sale->getAmount()->getTotal(),
					'extra'          => $payment->toJSON()
		      	];

			}


			return [
				'success'        => false,
				'transaction_id' => 'X-X-X-X-X',
				'status'         => $payment->getState(),
				'amount'         => $transactions[0]->getAmount()->getTotal(),
				'extra'          => $payment->toJSON()
	      	];

		} catch(\Exception $e){


			return [
				'success'        => false,
				'transaction_id' => 'X-X-X-X-X',
				'status'         => 'failed',
				'amount'         => 0.0,
				'extra'          => [
					'try_catch_error_message' => $e->getMessage()
				]
	      	];

		}
		
 
	}


}