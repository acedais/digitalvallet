<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\Api as ApiRepo;
use Illuminate\Http\Request;

class Api
{
    
    protected $auth;
    public function __construct(ApiRepo $api)
    {
        $this->api = $api;
    }


    public function handle(Request $request, Closure $next)
    {

        $access_token = $this->api->getAccessToken($request->header('Authorization'));

        if($user = $this->shouldPassThrough($access_token)) {
            $request->request->add(['auth_user' => $user]);
            return $next($request);
        }

        return response()->json(
            $this->api->createResponse(
                false, "UNAUTHORIZED_ACCESS", 'Unauthorized accesss'
            )
        );
    }


    protected function shouldPassThrough($access_token) 
    {
        return $this->api->shouldPassThrough($access_token);
    }


}
