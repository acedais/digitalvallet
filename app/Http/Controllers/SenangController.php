<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Setting;
use App\Models\Order;
use App\Models\Transaction;
use App\Repositories\Otp;
use App\Models\User;
use DB;


class SenangController extends Controller
{

	public function __construct(Api $api, Setting $setting, Order $order, Transaction $transaction, User $user, Otp $otp)
    {
        $this->api = $api;
        $this->setting = $setting;
        $this->order = $order;
        $this->transaction = $transaction;
        $this->user = $user;
        $this->otp = $otp;
    }

    public function getDetails()
    {
    	return $this->setting->get('senangpay_secret_key');
    }

    public function getSenangInit(Request $request, Setting $setting)
    {

        $authUser = $request->auth_user;
        $merchantKey = $this->setting->get('senangpay_merchant_id');
        $secretKey = $this->setting->get('senangpay_secret_key');
        $detail = 'Senangpay_add_money_'.$request->amount;


        $url_root = 'https://app.senangpay.my/payment/'.$merchantKey;



        $t = new $this->transaction;
        $t->trans_id = 0;
        $t->amount = $request->amount;
        $t->gateway = 'SENANGPAY';
        $t->extra_info = '';
        $t->status = 'PENDING';

        DB::beginTransaction();

        $t->save();


        $order = new $this->order;
        $order->user_id = $authUser->id;
        $order->trans_table_id = $t->id;
        $order->other_user_id = 0;
        $order->amount = $request->amount;
        $order->order_type = Order::ADD_MONEY;
        $order->remarks = $request->has('remarks') ? $request->remarks : '';
        $order->status = 'PENDING';
        $order->status_reason = 'WAITING_FOR_SANANGPAY';        
        $order->save();

        DB::commit();

        //DOLLAR TO MALAYSIA
        $rate = \Swap::latest('USD/MYR');
        $amount = $rate->getValue() * $request->amount;
        $amountInMYR = number_format($amount,2);

        $hashed_string = md5($secretKey.$detail.$amountInMYR.$order->id);

        $statusReason = [
            'reason' => 'WAITING_FOR_SANANGPAY',
            'hash' => $hashed_string
        ];

        $order->status_reason = json_encode($statusReason);
        $order->save();

        
      


        $data = [
            'amount' => $amountInMYR,       //$request->amount    convert to rm
            'detail' => $detail,
            'order_id' => $order->id,
            'name' => $authUser->fullName(),
            'email' => $authUser->email,
            'phone' => $authUser->fullMobileno(),
            'hash' => $hashed_string

        ];


        return response()->json(
            $this->api->createResponse(true, 'SENANGPAY_ORDER_CREATED', 'Senangpay order created', [
                'url' => $url_root,
                'params' => $data
            ])
        );

    }

    public function getSenangpayResponse(Request $request, Setting $setting, Api $api)
    {

        $secretKey = $this->setting->get('senangpay_secret_key');
        $responseHash = md5($secretKey.$request->status_id.$request->order_id.$request->transaction_id.$request->msg);

        $order = $this->order->where('id', '=', $request->order_id)->first();
        

        if(!$order) {
            return response()->json($this->api->createResponse(true, 'INVALID_ORDER', 'Invalid order', []));
        }

        $transaction = $this->transaction->where('id', $order->trans_table_id)->first();

        if(!$transaction) {
            return response()->json($this->api->createResponse(true, 'INVALID_ORDER', 'Invalid order', []));
        }

        $transaction->trans_id = $request->transaction_id;


        if($responseHash != $request->hash) {

            $transaction->status = 'failed';
            $transaction->save();

            $order->status = Order::FAILED;
            $order->status_reason = 'ADD_BALANCE_FAILED_WITH_SENANGPAY';
            $order->save();

            $user = $this->user->find($order->user_id);

            $msg = app('App\Repositories\Util')->msgReplace([], config('otp_messages.balance_add_failed'));

            $this->otp->sendMessage(
                $user->fullMobileno(), $msg
            );


            return response()->json($this->api->createResponse(true, 'HASH_MISMATCHED', 'Hash mismatched', []));
        }


            
        if($request->status_id == 1){

            $order->status = Order::SUCCESS;
            $order->status_reason = 'ADD_BALANCE_SUCCESS_WITH_SENANGPAY';
            $order->save();

            $transaction->status = 'succeeded';
            $transaction->save();

            $user = $this->user->find($order->user_id);
            $user->balance += $order->amount;
            $user->save();

            $msg = app('App\Repositories\Util')->msgReplace(
                [
                    '{{amount}}'   => $order->amount,
                    '{{currency}}' => $user->currency
                ],
            config('otp_messages.balance_add')
            );

            $this->otp->sendMessage(
                $user->fullMobileno(), $msg
            );

            echo('Payment Successful wait for a while to redirect');
            header('Refresh: 3;url=/passbook');



        } else {

            
            $transaction->status = 'failed';
            $transaction->save();

            $order->status = Order::FAILED;
            $order->status_reason = 'ADD_BALANCE_FAILED_WITH_SENANGPAY';
            $order->save();

            $user = $this->user->find($order->user_id);

            $msg = app('App\Repositories\Util')->msgReplace([], config('otp_messages.balance_add_failed'));
                
            $this->otp->sendMessage(
                $user->fullMobileno(), $msg
            );

            echo('Payment Failed wait for a while to redirect');
            header('Refresh: 3;url=/passbook');


        }
            


    }


}