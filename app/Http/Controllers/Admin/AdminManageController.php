<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Repositories\Admin;
use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AdminManageController extends Controller
{


    public function __construct(Api $api, Admin $admin)
    {
        $this->api = $api;
        $this->admin = $admin;
        $this->auth_admin = Auth::admin()->get();
    }



    public function showAdmins()
    {
        return view('admin.admins', [
            'admins' => $this->admin->allAdmins()
        ]);
    }




    public function deleteAdmin(Request $request)
    {
        $success = $this->admin->deleteAdminByID($this->auth_admin, $request->id);

        if($success) {
            return response()->json(
                $this->api->createResponse(true, 'DELETED', 'Admin deleted successfully')
            );
        } else {
            return response()->json(
                $this->api->createResponse(false, 'DELETE_FAILED', 'Admin delete failed')
            );
        }

    }



    public function updateAdmin(Request $request)
    {
        $data = [
            'name'         => $request->name, 
            'email'        => $request->email, 
            'role'         => $request->role, 
            'purpose'      => $request->purpose, 
            'country_code' => $request->country_code, 
            'mobile_no'    => $request->mobile_no, 
        ];

        if($request->has('password') &&  $request->password != '') {
            $data['password'] = \Hash::make($request->password);
        }

        $success = $this->admin->updateAdmin($request->id, $data);


        if($success) {
            return response()->json(
                $this->api->createResponse(true, 'UPDATED', 'Admin updated successfully')
            );
        } else {
            return response()->json(
                $this->api->createResponse(false, 'UPDATE_FAILED', 'Admin update failed')
            );
        }


    }


}
