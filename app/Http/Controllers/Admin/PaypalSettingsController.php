<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Setting;


class PaypalSettingsController extends Controller
{

    public function __construct(Setting $setting, Api $api)
    {
        $this->setting = $setting;
        $this->api = $api;
    }

  

    public function showPaypalSettings()
    {
        return view('admin.paypal_settings', [
            'paypal_client_id'   => $this->setting->get('paypal_client_id'),
            'paypal_secret_key' => $this->setting->get('paypal_secret_key'),
            'paypal_live_mode' => $this->setting->get('paypal_live_mode')
        ]);
    }



    public function savePaypalSettings(Request $request)
    {
        $this->setting->set('paypal_client_id', $request->paypal_client_id);
        $this->setting->set('paypal_secret_key', $request->paypal_secret_key);
        $this->setting->set('paypal_live_mode', $request->paypal_live_mode);
        return response()->json(
            $this->api->createResponse(true, 'PAPYPAL_SETTINGS_SAVED', 'Paypal settings saved')
        );
    }



}
