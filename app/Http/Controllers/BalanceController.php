<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Repositories\Balance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BalanceController extends Controller
{


    public function __construct(Balance $balance, Api $api)
    {
        $this->balance = $balance;
        $this->api = $api;
    }


    public function getBalance(Request $request)
    {
        $balance = $this->balance->formatAmountDecimalTwo($request->auth_user->balance);
        return response()->json(
            $this->api->createResponse(true, 'BALANCE_FETCHED', 'User balance fetched', ['balance' => $balance])
        );
    }


    public function transferBalance(Request $request)
    {

        if($request->has('country_code')) {
            $request->country_code = '+'.str_replace('+', '', $request->country_code);
        }

        if(!($this->balance->validateBalanceTransferData($request->all(), $errors))) {
            return response()->json(
                $this->api->createResponse(false, 'VALIDATION_ERROR', 'Validation error', $errors)
            );
        }

        $authUser = $request->auth_user;
        $success = $this->balance->transferAmount(
            $authUser, 
            $request->receiver_mobile, 
            $request->country_code,
            $request->amount,
            $request->remarks,
            $error
        );

        if($success) {

            return response()->json(
                $this->api->createResponse(true, 'SUCCESS', 'Balance transfer successful', [
                    'order_id' => '#'.$authUser->order_id,
                    'balance' => $authUser->balance
                ])
            );

        } else {

            return response()->json(
                $this->api->createResponse(false, $error['type'], $error['message'])
            );
        }
        
    }







    /* add balance with stripe */
    public function addBalanceWithStripe(Request $request)
    {

        $request->request->add(['gateway' => 'STRIPE']);
        $request->request->add(['description' => 'Add money to DigitalWallet account']);

        $success = $this->balance->addbalance($request->all(), $responseData);
        
        if($success) {
            return response()->json(
                $this->api->createResponse(true, 'BALANCE_ADDED_SUCCESS', 'Balance added successfully', $responseData)
            ); 
        } else {
            
            return response()->json(
                $this->api->createResponse(false, 'BALANCE_ADDED_FAILED', 'Balance added failed', $responseData)
            ); 
        }


    }




    public function addBalanceWithPaypal(Request $request)
    {
        $request->request->add(['gateway' => 'PAYPAL']);
        $request->request->add(['description' => 'Add money to DigitalWallet account']);

        $success = $this->balance->addbalance($request->all(), $responseData);
        
        if($success) {
            return response()->json(
                $this->api->createResponse(true, 'BALANCE_ADDED_SUCCESS', 'Balance added successfully', $responseData)
            ); 
        } else {
            
            return response()->json(
                $this->api->createResponse(false, 'BALANCE_ADDED_FAILED', 'Balance added failed', $responseData)
            ); 
        }
    }




    public function getTransactionDetails(Request $request)
    {
        $authUser = $request->auth_user;
        $tDetails = $this->balance->getTransactionDetails($authUser);
       
        return response()->json(
            $this->api->createResponse(true, 'T_DETAILS_FETCHED', 'Transaction details fetched successfully', $tDetails)
        );
    }


}
