<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use App\Repositories\Email;
use Hash;
use App\Repositories\Otp;
use App\Repositories\RegisterRepository;
use Illuminate\Http\Request;

class RegisterController extends Controller
{


    public function __construct(RegisterRepository $registerRepo, Api $api, Otp $otp, Email $email)
    {
        $this->registerRepo = $registerRepo;
        $this->api = $api;
        $this->otp = $otp;
        $this->email = $email;
    }

   
    public function doRegister(Request $request)
    {
        \Log::info($request->all());
        if($request->has('country_code')) {

            $request->country_code = '+'.str_replace('+', '', $request->country_code);
        }

        if(!$this->registerRepo->validateRegisterData($request->all(), $errors)) {
            return response()->json(
                $this->api->createResponse(false, 'VALIDATION_ERROR', 'Validation failed', $errors)
            );
        } 

       /* $mobile_no = $request->country_code.$request->mobile_no;
        if(!$this->otp->verifyOTP('REGISTRATION', $mobile_no, $request->otp_code)) {
            return response()->json(
                $this->api->createResponse(false, 'OTP_VERIFY_FAILED', 'Otp code is not valid')
            );
        }*/ 


        $user = $this->registerRepo->createUser([
            'first_name'       => $request->first_name,
            'last_name'        => $request->last_name,
            'mobile_no'        => $request->mobile_no,
            'country_code'     => $request->country_code,
            'password'         => Hash::make($request->password),
            'email'            => $request->email,
            'last_access_time' => date('Y-m-d H:i:s'),
            'last_access_ip'   => $request->ip()
        ]);

        if(!$user) {
            return $this->api->unknownErrResponse();
        }


        /*$this->otp->sendMessage($mobile_no, config('otp_messages.register_success'));*/
        $this->email->sendRegistrationEmail($user);


        $accessToken = $this->api->createAndSaveAccessToken($user->id);

        return response()->json(
            $this->api->createResponse(
                true, 'USER_REGISTERED', 'User registered successfully', [
                'access_token' => $accessToken->token,
                'user' => $user
            ])
        );

    }


    public function doLogin(Request $request)
    {
        if(!$this->registerRepo->validateLoginData($request->all(), $errors)) {
            return response()->json(
                $this->api->createResponse(false, 'LOGIN_FAILED', 'Login failed', $errors)
            );
        }

        $isLoginByEmail = ($request->has('login_by_email') 
                    && $request->login_by_email == 1) 
                    ? true : false;

        $username = ($isLoginByEmail) ? $request->email : $request->mobile_no;
        $password = $request->password;

        if(!($user = $this->registerRepo->login($username, $password, $isLoginByEmail))) {
            return response()->json(
                $this->api->createResponse(false, 'LOGIN_FAILED', 'Login failed', [
                    'password' => 'Wrong password'
                ])
            );
        }

        $accessToken = $this->api->createAndSaveAccessToken($user->id);

        return response()->json(
            $this->api->createResponse(
                true, 'LOGIN_SUCCESS', 'User logged in successfully', [
                'access_token' => $accessToken->token,
                'is_data_incomplete' => $this->registerRepo->isDataIncomplete($user),
                'user' => $user
            ])
        );

    }





    public function updateUser(Request $request)
    {
        $authUser = $request->auth_user;

        if($request->has('country_code')) {
            $request->country_code = '+'.str_replace('+', '', $request->country_code);
            $request->request->add(['country_code' => $request->country_code]);
        }

        $success = $this->registerRepo->updateUser($authUser, $request->all(), $resData);

        if($success) {


            return response()->json(
                $this->api->createResponse(
                    true, 'UPDATED', 'Profile updated successfully', $resData
                )
            );

        } else {

            return response()->json(
                $this->api->createResponse(
                    false, 'UPDATE_FAILED', 'Profile update failed', $resData
                )
            );

        }

    }









}
