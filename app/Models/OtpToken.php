<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtpToken extends Model
{
    protected $table = 'otp_tokens';
    protected $fillable = [
        'token', 
        'mobile_no',
        'expired_at',
        'token_type'
    ];

    public function getTableName()
    {
        return $this->table;
    }

}