<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialLogin extends Model
{
    protected $table = 'social_logins';
    protected $fillable = [
        'user_id', 
        'social_account_id',
        'social_provider'
    ];

    public function getTableName()
    {
        return $this->table;
    }

}