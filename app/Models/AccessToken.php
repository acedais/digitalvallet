<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    protected $table = 'access_tokens';
    protected $fillable = [
        'token', 
        'entity_id',
        'entity_type',
        'status'
    ];

    public function getTableName()
    {
        return $this->table;
    }

}