<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FitbitUserStepGoal extends Model
{
    protected $table = 'fitbit_users_step_goals';
    protected $fillable = [
        'user_id', 
        'step_log_date',
        'step_count'
    ];

    public function getTableName()
    {
        return $this->table;
    }

}  