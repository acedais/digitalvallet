<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    const ADD_MONEY     = 'ADD_MONEY';
    const SEND_MONEY    = 'SEND_MONEY';
    const BANK_TRANSFER = 'BANK_TRANSFER';
    const SUCCESS       = 'SUCCESS';
    const FAILED        = 'FAILED';
    const PENDING       = 'PENDING';

    protected $table = 'orders';
    protected $fillable = [
        'user_id', 
        'other_user_id',
        'trans_table_id',
        'amount',
        'currency',
        'order_type',
        'status',
        'status_reason',
        'remarks'
    ];


    public function getTableName()
    {
        return $this->table;
    }


    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'other_user_id');
    }



    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }


    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'trans_table_id');
    }


    public function transactionID()
    {
        return $this->transaction ? $this->transaction->trans_id : '';
    }


    public function transactionGateway()
    {   
        return $this->transaction ? $this->transaction->gateway : '';
    }


    public function transactionDate()
    {
        return date('d.m.Y', strtotime($this->created_at));
    }

}