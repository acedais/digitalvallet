<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FitbitUser extends Model
{
    protected $table = 'fitbit_users';
    protected $fillable = [
        'fitbit_refresh_token', 
        'user_id',
        'fitbit_user_id'
    ];

    public function getTableName()
    {
        return $this->table;
    }

}  