<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 256);
            $table->string('last_name', 256);
            $table->string('password', 512);
            $table->string('mobile_no', 30)->nullable();
            $table->string('country_code', 20);
            $table->tinyInteger('is_mobile_verified')->default(0); 
            $table->string('email', 256)->nullable();    //
            $table->decimal('balance', 10, 2)->default(0.00);
            $table->enum('currency', ['USD', 'MYR'])->default('USD');
            $table->enum('register_from', ['NORMAL', 'FACEBOOK', 'GOOGLE'])->default('NORMAL');
            $table->enum('data_incomplete', [1, 0])->default(0);
            $table->enum('status', ['ACTIAVATED', 'DEACTIVATED_BY_HIMSELF', 'DEACTIVATED_BY_ADMIN'])->default('ACTIAVATED');
            $table->timestamp('last_access_time');
            $table->string('last_access_ip', 50);

            $table->decimal('saving_amount', 10, 2)->default(0.00);


            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
