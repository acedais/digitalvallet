<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNotificationTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notification_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('device', 256)->nullable();
            $table->bigInteger('entity_id');
            $table->enum('entity_type', ['USER', 'ADMIN'])->default('USER');
            $table->enum('status', ['VALID', 'EXPIRED'])->default('VALID');

            $table->timestamps();
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('push_notification_tokens', function (Blueprint $table) {
            //
        });
    }
}
