<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitbitUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitbit_users', function (Blueprint $table) {
           
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('fitbit_user_id', 128);
            $table->string('fitbit_refresh_token', 256);
         
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
