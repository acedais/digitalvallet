<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtpTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otp_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token', 256);
            $table->string('mobile_no', 30);
            $table->timestamp('expired_at');
            $table->enum('token_type', ['LOGIN', 'REGISTRATION', 'PASSWORD_RECOVERY', 'USER_DATA_UPDATE'])->default('REGISTRATION');


            $table->timestamps();
            $table->softDeletes();
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('otp_tokens', function (Blueprint $table) {
            //
        });
    }
}
