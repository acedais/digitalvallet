<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('admins', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->string('name', 200)->nullable();
            $table->string('email', 100)->unique();
            $table->string('password', 1000);
            $table->enum('role', ['root', 'guest'])->default('root');
            $table->string('purpose', 100)->nullable();
            $table->string('country_code', 10)->nullable();
            $table->string('mobile_no', 30)->nullable();
            $table->string('last_ip', 50);
            $table->timestamp('last_login');
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
