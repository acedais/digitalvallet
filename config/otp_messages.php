<?php

return [

	"registration"              => "Your alicia wallet registration one time password is : {{otp_code}}",
	"register_success"          => "Welcome to Alicia Wallet. Thanks for registering with us.",
	'balance_transfer_sender'   => 'Amount {{amount}} {{currency}} has been transferred to {{mobile}} successfully from your account',
	'balance_transfer_receiver' => 'Amount {{amount}} {{currency}} has been added to your Alicia wallet account({{mobile_to}}) from {{name}}({{mobile_from}})',
	'balance_add'               => 'Amount {{amount}} {{currency}} added to your Alicia Wallet account',
	'balance_add_failed'        => 'You Alicia Wallet balance add failed',
	"password_recovery"         => "Your Alicia Wallet password recovery one time password is : {{otp_code}}",
	'setp_goal_trasfer_amount_to_saving' => '{{amount}} {{currency}} has been transferred to your savings account from your Alicia Wallet {{extrainfo}}' 
];