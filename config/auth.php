<?php

return [
    
    'multi-auth' => [
        'admin' => [
            'driver' => 'eloquent',
            'model'  => App\Models\Admin::class
        ]
    ]
    
];
