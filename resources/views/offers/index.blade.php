<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Paytm</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <style></style>
</head>

<body>
    <div class="home-demo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="owl-carousel">
                        <div class="item">
                            <a href=" https://www.cleartrip.com/" target="_blank"> <img src="images/Banner1.png" /></a>
                        </div>
                        <div class="item">
                            <a href="https://www.bookmyshow.com/" target="_blank"> <img src="images/Banner2.png" /></a>
                        </div>
                        <div class="item">
                            <a href="https://freshmenu.com/" target="_blank"> <img src="images/Banner4.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-demo incredible_offers">
        <p class="text-center">Top Electronics Offers ></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="owl-carousel">
                        <div class="item">
                            <a target="_blank" href="https://www.amazon.com/Amazon-Echo-Dot-Portable-Bluetooth-Speaker-with-Alexa-Black/dp/B01DFKC2SO/ref=sr_1_1?ie=UTF8&qid=1493882103&sr=8-1&keywords=echo+dot">
                                <div class="img_otr"><img src="images/offer_two.png" class="offer_one_img img-responsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">Echo Dot</p>
                                <p>$49.99</p>
                            </div>
                        </div>
                        <div class="item">
                            <a target="_blank" href=" https://www.amazon.com/dp/B00ISSD6QG/ref=s9_acsd_bw_wf_s_NRwaterf_mdl?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-3&pf_rd_r=D1AR8W7694Z5BMRVPSPR&pf_rd_t=101&pf_rd_p=446f181d-01dd-43df-a0b5-ec35cf82056f&pf_rd_i=10711515011">
                                <div class="img_otr"><img src="images/offer_one.png" class="offer_two_img img-responsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">Fitbit Flex</p>
                                <p>$62.99</p>
                            </div>
                        </div>
                        <div class="item">
                            <a target="_blank" href="https://www.amazon.com/Bang-Olufsen-Portable-Bluetooth-Microphone/dp/B01KPSJV92/ref=lp_16609790011_1_6?ie=UTF8&qid=1493882456&sr=8-6">
                                <div class="img_otr"><img src="images/offer_three.png" class="offer_three_img img-repsonsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">B&O Play</p>
                                <p>$249</p>
                            </div>
                        </div>
                        <div class="item">
                            <a target="_blank" href="https://www.amazon.com/Bluetooth-Headphones-Waterproof-Sweatproof-Cancelling/dp/B01G8JO5F2/ref=lp_2407776011_1_1?s=wireless&ie=UTF8&qid=1493882446&sr=1-1">
                                <div class="img_otr"> <img src="images/offer_four.png" class="offer_four_img img-responsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">SENSO Headphones</p>
                                <p>36.97</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-demo incredible_offers">
        <p class="text-center">Incredible Shopping Offers ></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="owl-carousel">
                        <div class="item">
                            <a target="_blank" href=" https://www.groupon.com/goods/electronics">
                                <div class="img_otr"><img src="images/home-theater-tv.png" class="deal_one_img img-responsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">Today's Deals</p>
                            </div>
                        </div>
                        <div class="item">
                            <a target="_blank" href="https://www.amazon.com/s/?rh=n%3A7141123011%2Cn%3A10445813011%2Cn%3A7147440011%2Cn%3A1040660%2Cn%3A1045024&bbn=10445813011&pf_rd_p=b0887c79-106c-4b4f-bdf6-001a95f810f1&pf_rd_r=HJ9JSX4SP8QGT8NGE6KB">
                                <div class="img_otr"><img src="images/Model.png" class="deal_two_img img-responsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">Woman's Dress</p>
                            </div>
                        </div>
                        <div class="item">
                            <a target="_blank" href="https://www.amazon.com/books-used-books-textbooks/b/ref=nav_shopall_bo_t1?ie=UTF8&node=283155">
                                <div class="img_otr"><img src="images/Book-Transparent-Background.png" class="deal_three_img img-repsonsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">Books</p>
                            </div>
                        </div>
                        <div class="item">
                            <a target="_blank" href="https://www.amazon.com/Sports-Collectibles/b/ref=nav_shopall_sa_sp_sptcllct?ie=UTF8&node=3250697011">
                                <div class="img_otr"><img src="images/multi-sports.png" class="deal_four_img img-responsive" style="" /></div>
                            </a>
                            <div class="text_block">
                                <p class="help-block">Sports Collectibles</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 5,
            loop: false,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        })
    </script>
    <script src="js/highlight.js"></script>
    <script src="js/app.js"></script>
</body>

</html>