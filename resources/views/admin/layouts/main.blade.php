<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{{ csrf_token() }}}">
        <title>PayNow</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{{asset('admin_assets')}}}/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{{asset('admin_assets')}}}/plugins_assets/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="{{{asset('admin_assets')}}}/css/AdminLTE.min.css">
        <link rel="stylesheet" href="{{{asset('admin_assets')}}}/css/skins/_all-skins.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/plugins/iCheck/polaris/polaris.css')}}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link href="{{{asset('admin_assets')}}}/css/toastr.css" rel="stylesheet" />
        <style type="text/css">
            .skin-blue .sidebar-menu>li>.treeview-menu {
            background: none !important;
            }
            .treeview-menu
            {
            margin-top: -1px !important;
            margin-left: -1px !important;
            }
            .label--checkbox {
            position: relative;
            margin: .5rem;
            font-family: Arial, sans-serif;
            line-height: 135%;
            cursor: pointer;
            margin-bottom: -5px;
            }
            .checkbox {
            position: relative;
            top: -0.375rem;
            margin: 0 1rem 0 0;
            cursor: pointer;
            width: 16px;
            height: 16px;
            }
            .checkbox:before {
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition: all 0.1s ease-in-out;
            transition: all 0.1s ease-in-out;
            content: "";
            position: absolute;
            left: 0;
            z-index: 1;
            width: 1rem;
            height: 1rem;
            border: 2px solid #f2f2f2;
            visibility: hidden;
            }
            .checkbox:checked:before {
            -webkit-transform: rotate(-45deg); */
            -moz-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
            transform: rotate(-45deg);
            height: 13px;
            border-color: #009688;
            border-top-style: none;
            border-right-style: none;
            width: 22px;
            border-bottom: 3px solid #009688;
            border-left: 3px solid #009688;
            top: -5px;
            left: 2px;
            /*border-radius: 0px 0px 6px 0px;*/
            visibility: visible;
            }
            .checkbox:after {
            content: "";
            position: absolute;
            top: -0.125rem;
            left: 0;
            width: 16px;
            height: 17px;
            background: #fff;
            cursor: pointer;
            border: 1px solid rgba(0, 0, 0, 0.2);
            }
            .margin-top-5px
            {
            margin-top: 5px;
            }
            /*.select2-container .select2-selection--single .select2-selection__rendered
            {
            margin-top: -7px;
            }*/
            .select2-container .select2-selection--single
            {
            height: 34px;
            }
            .skin-blue .sidebar-menu>li>.treeview-menu
            {
            background: #2C3B3E !important
            }
            .user-img-custom
            {
            width: 50px;
            height: 50px;
            background-size: cover !important;
            background-repeat: no-repeat !important;
            /*border-radius: 50%;
            box-shadow: 2px 2px 6px 4px rgba(0, 0, 0, 0.12);*/
            }
            .no-background-color
            {
            background-color: transparent !important;
            }
            .no-border-color
            {
            border-color: transparent !important;
            }
            .modal-close-btn
            {
            color: black;
            opacity: 1;
            }
            .text-uppercase
            {
            text-transform: uppercase;
            }
            .height-150px
            {
            height: 150px !important;
            }
            .alert-close-btn
            {
            position: absolute;
            right: 7px;
            top: 5px;
            cursor: pointer;
            }
            .position-relative
            {
            position: relative;
            }
            .text-center
            {
            text-align: center;
            }
            .margin-0px
            {
            margin: 0px;
            }
            .text-right
            {
            text-align: right;
            }
        </style>
        @section('top-scripts')
        @show
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="" style="width:32px;height:32px"></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>PayNow</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar sidebar-custom">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header admin-header-custom"></li>
                        <li class="active">
                            <a href="{{{ url('admin/dashboard') }}}">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                            <i class="fa fa-users"></i>
                            <span>User & Admin</span><i class="fa fa-caret-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{{ url('admin/users') }}}"><i class="fa fa-circle-o"></i>Users</a></li>
                                <li><a href="{{{url('admin/admin-management')}}}"><i class="fa fa-circle-o"></i>Admins</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                            <i class="fa fa-usd"></i> <span>Economics</span>
                            <i class="fa fa-caret-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{{ url('admin/users/transactions') }}}"><i class="fa fa-circle-o"></i>User Transactions</a></li>
                                <!-- <li><a href="{{{ url('/admin/financeSettings') }}}"><i class="fa fa-circle-o"></i>Financial Statistics</a></li> -->
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <!--Dummy Link-->
                                <i class="glyphicon glyphicon-cog"></i> <span>Settings</span>
                                <i class="fa fa-caret-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{url('admin/settings/twilio')}}"><i class="fa fa-circle-o"></i>Twilio</a></li>
                                <li><a href="{{url('admin/settings/facebook')}}"><i class="fa fa-circle-o"></i>Facebook</a></li>
                                <!-- <li><a href="{{url('admin/settings/Google') }}}"><i class="fa fa-circle-o"></i>Google</a></li> -->
                                <li><a href="{{{ url('admin/settings/email') }}}"><i class="fa fa-circle-o"></i>Email</a></li>
                                <li><a href="{{{ url('admin/settings/paypal') }}}"><i class="fa fa-circle-o"></i>Paypal</a></li>
                                <li><a href="{{{ url('admin/settings/stripe') }}}"><i class="fa fa-circle-o"></i>Stripe</a></li>
                                <li><a href="{{{ url('admin/settings/senangpay') }}}"><i class="fa fa-circle-o"></i>Senangpay</a></li>
                                <li><a href="{{{ url('admin/settings/fitbit') }}}"><i class="fa fa-circle-o"></i>Fitbit</a></li>
                                <li><a href="{{{ url('admin/settings/ifttt') }}}"><i class="fa fa-circle-o"></i>IFTTT</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="{{{ url('admin/logout') }}}">
                            <i class="fa fa-power-off"></i> <span>Logout</span></i>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            @section('content') @show
        </div>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/js/bootstrap.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/fastclick/fastclick.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/js/app.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/sparkline/jquery.sparkline.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/plugins_assets/chartjs/Chart.min.js"></script>
        <script src="{{{asset('admin_assets')}}}/js/demo.js"></script>
        <script src="{{{asset('admin_assets')}}}/js/toastr.js"></script>
        <script src="{{asset('admin_assets/plugins/iCheck/icheck.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            
                $(document).ready(function() {
                    $(".select-custom").select2();
                });
            
                toastr.options = {"closeButton": true};
            
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }); 
            
            
                console.log("%cStop!! \n%cThis is a browser feature intended for developers. If someone told you to copy and paste something here to enable a DatingFramework feature, it is a scam and will give them access to your DatingFramework admin panel", "color: red; font-size:32px;font-weight:700","color: black; font-size:16px;font-weight:700");
            
            });
        </script>
        @section('scripts') @show
    </body>
</html>
