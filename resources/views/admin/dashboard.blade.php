@extends('admin.layouts.main')
@section('content')
@parent
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Dashboard
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>User Statistics</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-4 col-xs-6 col-sm-4 col-md-4">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{{$total_users_count}}}</h3>
                                    <p>Total User</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6 col-sm-4 col-md-4">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{$total_this_month_users_count}}</h3>
                                    <p>This Month</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6 col-sm-4 col-md-4">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{$total_today_users_count}}</h3>
                                    <p>Today</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>User Global Statistics</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div id="country-users" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript"
    src="https://www.google.com/jsapi?autoload={
    'modules':[{
    'name':'visualization',
    'version':'1',
    'packages':['corechart']
    }]
    }"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["geochart"]});
    google.setOnLoadCallback(drawRegionsMap);
    
    function drawRegionsMap() {
    
      var data = google.visualization.arrayToDataTable([
    
        ['Country', 'Users'],
        @if(count($country_signups) > 0)
          @foreach($country_signups as $country => $count)
            ['{{$country}}', {{$count}}],
          @endforeach
        @endif
    
      ]);
    
      var options = {
          backgroundColor: '#FFFFF',
          chartArea: {
                    backgroundColor: '#FFFFF'
                }};
    
      var chart = new google.visualization.GeoChart(document.getElementById('country-users'));
    
      chart.draw(data, options);
    }
    
    
    
    
</script>
@endsection