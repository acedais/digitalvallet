@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Facebook Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Facebook Api Key Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "facebook-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Facebook App ID</label>
                                <input type="text"  placeholder="Enter Facebook App ID" name = "facebook_app_id" value = "{{$facebook_app_id}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Facebook App Secret</label>
                                <input type="text" placeholder="Enter Facebook App Secret" name = "facebook_app_secret" value = "{{$facebook_app_secret}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Test Facebook</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <textarea class="form-control" id="test-response" style="height:350px"></textarea>
                        <button type="button" class="btn btn-block btn-success btn-flat margin-top-5px" id="test-fb-btn">TEST</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    
    
        $("#facebook-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/facebook/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });
    
    
    
        var fb_access_token = '';
        var fb_app_id = '';
    
        $.ajaxSetup({ cache: true });
    
    
        function FacebookLogin() {
    
            FB.init({
                appId: fb_app_id,
                version: 'v2.5' // or v2.0, v2.1, v2.2, v2.3
            });  
    
    
            FB.login(function(response) {
                
                if (response.status = "connected") {    
                    if(response.authResponse) {
                        runTest(response.authResponse.accessToken);
                    }
                } else {
                    toastr.error('Authrization failed');
                }
            },{scope:'email, user_photos', return_scopes: true});
        }
    
    
        function runTest(auth_token)
        {
    
            var data = {
                auth_token : auth_token,
                _token : "{{csrf_token()}}"
            };
    
            $.post("{{url('admin/settings/facebook/test')}}", data, function(response){
    
                var prety = JSON.stringify(response.data, undefined, 4);
    
                $("#test-response").text(prety);
    
                toastr.info('Test completed');
    
            });
        }
    
    
    
    
        $("#test-fb-btn").on('click', function(){
    
            fb_app_id = $("input[name='facebook_app_id']").val();
            FacebookLogin();
    
        });
    
    
    
    
    
    });
    
    
</script>
@endsection