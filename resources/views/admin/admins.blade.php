@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Admins Management
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Admins</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="user-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Purpose</th>
                                    <th colspan="2" style="text-align:center;">Mobile</th>
                                    <th>Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($admins as $admin)
                                <tr id="admin-id-{{$admin->id}}">
                                    <td>#{{$admin->id}}</td>
                                    <td id="admin-name-{{$admin->id}}">{{$admin->name}}</td>
                                    <td id="admin-email-{{$admin->id}}">{{$admin->email}}</td>
                                    <td id="admin-role-{{$admin->id}}">{{$admin->role}}</td>
                                    <td id="admin-purpose-{{$admin->id}}">{{$admin->purpose}}</td>
                                    <td id="admin-ccode-{{$admin->id}}">{{$admin->country_code}}</td>
                                    <td id="admin-mobile-{{$admin->id}}">{{$admin->mobile_no}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-flat dropdown-toggle no-background-color no-border-color" data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-bars"></i>
                                            <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                <li class="action_each_menu_item" data-action-type="UPDATE" data-admin-id="{{$admin->id}}">
                                                    <a href="javascript:void(0)">UPDATE</a>
                                                </li>
                                                <li class="action_each_menu_item" data-action-type="DELETE" data-admin-id="{{$admin->id}}">
                                                    <a href="javascript:void(0)">DELETE</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
</div>
<div id="updateAdminModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">UPDATE ADMIN</h4>
      </div>
      <div class="modal-body">
        
            <form method = "POST" id = "admin-update-form">
                {!!csrf_field()!!}
                <input type="hidden" name="id" >
                <div class="form-group">
                    <label class="package-label">Name</label>
                    <input type="text" name = "name" class="form-control">
                </div>
                <div class="form-group">
                    <label class="package-label">Emaiil</label>
                    <input type="text" name = "email"class="form-control">
                </div>
                <div class="form-group">
                    <label class="package-label">Role</label>
                    <select name="role" class="select-custom form-control">
                        <option value="root">ROOT</option>
                        <option value="guest">GUEST</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="package-label">Purpose</label>
                    <input type="text" name = "purpose" class="form-control">
                </div>
                <div class="form-group">
                    <label class="package-label">Password</label>
                    <input type="password" name = "password" class="form-control">
                </div>
                <div class="form-group">
                    <label class="package-label">Country Code</label>
                    <input type="text" name = "country_code" class="form-control">
                </div>
                <div class="form-group">
                    <label class="package-label">Mobile No</label>
                    <input type="text" name = "mobile_no" class="form-control">
                </div>
                <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">UPDATE</button>
            </form>


      </div>
    
    </div>

  </div>
</div>
@endsection
@section('top-scripts')
<style type="text/css">
    .select2-container{
        width: 100% !important;
    }
</style>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){
        
        _token = "{{csrf_token()}}";



        function updateAdmin(id)
        {   
            $("#updateAdminModal").modal('show');

            $("input[name='name']").val( $("#admin-name-"+id).html() );
            $("input[name='email']").val( $("#admin-email-"+id).html() );
            $("input[name='purpose']").val( $("#admin-purpose-"+id).html() );
            $("input[name='mobile_no']").val( $("#admin-mobile-"+id).html() );
            $("input[name='country_code']").val( $("#admin-ccode-"+id).html() );
            $("input[name='id']").val( id );


            var role = $("#admin-role-"+id).html();
            $("select[name='role'] option[value='"+role+"']").prop('selected','selected').change();

        }

        $("#admin-update-form").on("submit", function(event){

            event.preventDefault();


            var data = $("#admin-update-form").serializeArray();
            $.post('{{url('admin/admin-management/admin/update')}}', data, function(response){

                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error(response.text);
                }

            });


        });


        




        function deleteAdmin(id)
        {
            var data = {
                _token : _token,
                id : id
            };
            $.post("{{url('admin/admin-management/admin/delete')}}", data, function(response){
                
                if(response.success) {
                    toastr.success(response.text);
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                } else {
                    toastr.error(response.text);
                }

            });
           
        }




        $(".action_each_menu_item").on('click', function(){

            var action_type = $(this).data('action-type');
            var admin_id = $(this).data('admin-id');

            if(action_type == 'DELETE') {
                deleteAdmin(admin_id);
            } else if(action_type == 'UPDATE') {
                updateAdmin(admin_id);
            }

        });

        
    });
    
</script>
@endsection