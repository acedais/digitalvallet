@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Paypal Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Paypal Api Key Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "paypal-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Paypal Client ID</label>
                                <input type="text"  placeholder="Enter Paypal Client ID" name = "paypal_client_id" value = "{{$paypal_client_id}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Paypal Secret Key</label>
                                <input type="text" placeholder="Paypal Secret Key" name = "paypal_secret_key" value = "{{$paypal_secret_key}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Paypal Live Mode</label>
                                <select name="paypal_live_mode" class="select-custom form-control">
                                    <option value="true" @if($paypal_live_mode == 'true') selected @endif>Yes</option>
                                    <option value="false" @if($paypal_live_mode !== 'true') selected @endif>No</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){

    
        $("#paypal-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/paypal/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });

    
    });
    
</script>
@endsection