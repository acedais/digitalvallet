@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            senangpay Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Senangpay Api Key Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "senangpay-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Senangpay Merchant ID</label>
                                <input type="text"  placeholder="Enter senangpay Merchant ID" name = "senangpay_merchant_id" value = "{{$senangpay_merchant_id}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Senangpay Secret Key</label>
                                <input type="text" placeholder="senangpay Secret Key" name = "senangpay_secret_key" value = "{{$senangpay_secret_key}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="package-label">Place this in Sanangpay Return URL and Callback URL </label>
                                <input type="text" id="url" placeholder="senangpay Return URL" name = "" value ="{{url('user/senangpay/response')}}" class="form-control" readonly>
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){

    
        $("#senangpay-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/senangpay/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });

    
    });

    //document.getElementById('url').value=window.location.host+'/user/senangpay/response';
    
</script>
@endsection