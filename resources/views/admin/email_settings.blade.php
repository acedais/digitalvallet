@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Email Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Email Driver</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" class = "mail-setting-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <select name="mail_driver" class="select-custom form-control">
                                    @if($mail_driver == null) 
                                    <option value="">--Choose--</option>
                                    @endif
                                    <option value="smtp" @if($mail_driver == 'smtp') selected @endif>SMTP</option>
                                    <option value="mandrill" @if($mail_driver == 'mandrill') selected @endif>MANDRILL</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Email Test</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "mail-test-form">
                            {!!csrf_field()!!}
                            <div class="form-group col-md-6">
                                <label class="package-label">Eamil ID</label>
                                <input type="text" name = "email_id" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="package-label">Message</label>
                                <input type="text" name = "body" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">TEST SEND</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>SMTP Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" class = "mail-setting-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Host</label>
                                <input type="text" name = "smtp_host" value = "{{$smtp_host}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Port</label>
                                <input type="text" name = "smtp_port" value = "{{$smtp_port}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Encryption</label>
                                <input type="text" name = "smtp_encryption" value = "{{$smtp_encryption}}" class="form-control" placeholder="eg. tls">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Username</label>
                                <input type="text" name = "smtp_username" value = "{{$smtp_username}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Password</label>
                                <input type="password" name = "smtp_password" value = "{{$smtp_password}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">From</label>
                                <input type="text" name = "smtp_from_address" value = "{{$smtp_from_address}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">From Name</label>
                                <input type="text" name = "smtp_from_name" value = "{{$smtp_from_name}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Mandrill Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" class = "mail-setting-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Host</label>
                                <input type="text" name = "mandrill_host" value = "{{$mandrill_host}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Port</label>
                                <input type="text" name = "mandrill_port" value = "{{$mandrill_port}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Username</label>
                                <input type="text" name = "mandrill_username" value = "{{$mandrill_username}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Secret</label>
                                <input type="password" name = "mandrill_secret" value = "{{$mandrill_secret}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){
    
    
        $(".mail-setting-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
            console.log(data);
            $.post("{{url('admin/settings/email/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });
    
    
    
        $("#mail-test-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
          
            $.post("{{url('admin/settings/email/test')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error(response.data.message);
                }
    
            });
    
    
        });
    
        
    
    
    });
    
</script>
@endsection