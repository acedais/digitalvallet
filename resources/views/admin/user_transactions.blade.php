@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Users Transactions
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Transaction Orders</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered" id="user-table">
                            <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>User Name</th>
                                    <th>Transaction ID</th>
                                    <th>Gateway</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Date</th>
                                    <th>Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>#{{$order->id}}</td>
                                    <td>{{$order->user_name}}
                                        <br>
                                        [{{$order->user_mobile}}]
                                    </td>
                                    <td>{{$order->transaction_id}}</td>
                                    <td> @if($order->gateway=='PAYPAL')
                                        <i class="fa fa-paypal font-size-20px" title="Payal"></i>
                                        @elseif($order->gateway=='STRIPE')
                                        <i class="fa fa-cc-stripe font-size-20px" title="stripe"></i>
                                        @endif
                                    </td>
                                    <td style="font-weight:700; @if($order->amount_deducted) color:red; @else color:green; @endif">{{$order->amount_string}}</td>
                                    <td>
                                        @if($order->status=='FAILED')
                                        <span style="color:red">Error</span>
                                        @else
                                        {{$order->type}}
                                        @endif
                                    </td>
                                    <td>{{$order->transaction_date}}</td>
                                    <td class="text-center;">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-flat dropdown-toggle no-background-color no-border-color" data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-bars"></i>
                                            <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                <li class="action_each_menu_item" data-action-type="ORDER_DETAILS" data-order-id="{{$order->id}}">
                                                    <a href="javascript:void(0)">DETAILS</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12" style="text-align:center">
                            {!! $orders->appends(request()->all())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="order_details_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">ORDER DETAILS</h4>
            </div>
            <div class="modal-body">
                <div class="loader-container">
                    <div class="loader"></div>
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="col-md-3">ORDER ID : </td>
                            <td id="m_order_id" class="col-md-3"></td>
                            <td class="col-md-3">USER : </td>
                            <td id="m_order_user" class="col-md-3"></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">ORDER TYPE : </td>
                            <td class="col-md-3" id="m_order_type"></td>
                            <td class="col-md-3">ORDER STATUS : </td>
                            <td class="col-md-3" id="m_order_status"></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">REMARKS : </td>
                            <td class="col-md-3" id="m_order_remarks"></td>
                            <td class="col-md-3">TRANSACTION ID : </td>
                            <td class="col-md-3" id="m_transaction_id"></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">ORDER DATE : </td>
                            <td class="col-md-3" id="m_order_date"></td>
                            <td class="col-md-3">GATEWAY : </td>
                            <td class="col-md-3" id="m_order_gateway"></td>
                        </tr>
                        <tr>
                            <td class="col-md-3">AMOUNT : </td>
                            <td class="col-md-3" id="m_order_amount"></td>
                            <td class="col-md-3">CURRENCY : </td>
                            <td class="col-md-3" id="m_order_currency"></td>
                        </tr>
                        <tr>
                            <td colspan="4">ORDER DESC: <span id="m_order_desc"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="panel-group extra-panel-group">
                    <div class="panel panel-default no-border">
                        <div class="panel-heading text-center bg-transparent no-border">
                            <i class="fa fa-chevron-down" data-toggle="collapse" href="#trans_extra" style="cursor:pointer;" title="Raw transaction response information"></i>
                        </div>
                        <div id="trans_extra" class="panel-collapse collapse">
                            <div class="panel-body">
                                <textarea id="extra_info" style="width: 100%;height: 300px;">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('top-scripts')
<style type="text/css">

    .loader-container
    {
        display: none;
        position: absolute;
        left: 0px;
        right: 0px;
        width: 100%;
        height: 100%;
        text-align: center;
        background: rgba(0, 0, 0, 0.07);
    }

    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        display: inline-block;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }

    .no-border
    {
    border: none !important;
    }
    .bg-transparent
    {
    background-color: transparent !important;
    }
    #order_details_modal .modal-dialog 
    {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    }
    #order_details_modal .modal-content 
    {
    height: auto;
    min-height: 100%;
    border-radius: 0;
    }
    #m_order_desc 
    {
    font-weight: initial;
    }
    #order_details_modal  table tbody tr td:nth-child(1)
    {
    font-weight: 700;
    /*  text-align: right;*/
    }
    #order_details_modal  table tbody tr td:nth-child(3)
    {
    font-weight: 700;
    }
    .borderless td, .borderless th {
    border: none !important;
    }
    .font-size-20px
    {
    font-size: 20px;
    }
    #user-table_filter > label
    {
    padding: 10px;
    /* background: rgba(0, 0, 0, 0.01);*/
    border-radius: 2px;
    margin-top: 5px;
    margin-left: 10px;
    font-weight: normal;
    margin-bottom: 12px;
    /*border: 1px solid rgba(0, 0, 0, 0.07);*/
    }
    #user-table_filter > label > input
    {
    margin-left: 5px;
    width: 300px;
    height: 44px;
    border: none;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
    transition: box-shadow 200ms cubic-bezier(0.4, 0.0, 0.2, 1);
    font: 16px arial,sans-serif;
    padding-left: 5px;
    }
    #user-table_filter > label > input:focus
    {
    outline: none;
    }
    .dataTables_empty
    {
    display: none;
    }
    #user-table_length, #user-table_info, #user-table_paginate
    {
    display: none;
    }
    .dropdown-menu > li > a
    {
    text-transform: uppercase;
    }
</style>
@endsection 
@section('scripts')
@parent
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#user-table').DataTable({
            "pageLength": 100,
            "order": [] 
        });
        
        function showLoader(show=true)
        {
            if(show) {
                $(".loader-container").fadeIn();
            }
            else{
                $(".loader-container").fadeOut();
            }
        }   
    
    
        function renderOrderDetails(order)
        {
            $("#m_order_id").html('#'+order.id);
            $("#m_transaction_id").html(order.transaction_id);
            $("#m_order_type").html(order.type);
            $("#m_order_date").html(order.transaction_date);
            $("#m_order_gateway").html(order.gateway);
            $("#m_order_amount").html(order.amount);
            $("#m_order_currency").html(order.currency);
            $("#m_order_user").html(order.user_name+' ('+ order.user_mobile +') ');
            $("#m_order_remarks").html(order.remarks);
            $("#m_order_status").html(order.status);
    
            
            if(order.type == 'ADDED') {
                $(".extra-panel-group").show();
                var parsed = JSON.parse(order.transaction.extra_info);
                try {
                    var parsed = JSON.parse(parsed);
                } catch(e){}
                
                var prety = JSON.stringify(parsed, undefined, 4);
                $("#extra_info").html(prety);
            } else {
                $(".extra-panel-group").hide();
            }
    
    
            var desc = '';
            if(order.status == 'FAILED') {
                desc = '';
            } else if(order.type == 'ADDED') {
    
                desc  = 'Amount ' + order.amount + ' $ added to wallet';
    
            } else if(order.type == 'PAID') {
    
                desc  = order.user_name + ' has transfered amount ' + order.amount + ' $ to ' + order.other_user_name +' ('+ order.other_user_mobile +') ';
    
            } else if(order.type == 'RECEIVED') {
                desc  = order.user_name + ' has received amount ' + order.amount + ' $ from ' + order.other_user_name +' ('+ order.other_user_mobile +') ';
            }
    
            $("#m_order_desc").html(desc);
    
    
    
        }
    
    
    
    
    
        $(".action_each_menu_item").on('click', function(){
    
            var action_type = $(this).data('action-type');
            var order_id = $(this).data('order-id');
            console.log(action_type);
        
            showLoader(true);
            $("#order_details_modal").modal('show');
            $.ajax({
                url: "{{url('admin/user/order')}}/"+order_id,
                type: 'GET',
                success: function(response){ 
                    if(response.success) {
                        console.log(response.data.order);
                        renderOrderDetails(response.data.order);
                        
                    } else {
                        toastr.error('Some error occoured');
                    }
                    showLoader(false);
                },
                error: function(data) {
                    toastr.error('Some error occoured');
                    showLoader(false);
                }
            });
    
    
    
    
        });
    
    
    
    
    });
</script>
@endsection