<!DOCTYPE html>
<html>
<head>
	<title>Regisration OTP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		html, body {
		    margin: 0;
		    padding: 0;
		    width: 100%;
		    height: 100%;
		    display: table;
		    background-color: rgb(206,206,206);
		}

		.container {
			display: table-cell;
		    text-align: center;
		    vertical-align: middle;
		}


		.content
		{
			    display: inline-block;
			    padding: 13px;
			    border: 5px solid teal;
			    background-color: white;
		}

		.body
		{
			text-align: left;
		    border: 1px solid black;
		    padding: 10px;
		}


	</style>
</head>
<body>
		
		<div class="container">
			
			<div class="content">
				<header>
					<h2 style="margin: 0px;">Digital Wallet</h2>
					<p style="margin: 0px 0px 0px 68px;">One Time Password</p>
				</header>
				<hr>
				<div class="body">
					<p>Hi, {{$user->first_name}}</p>
					<p>Your registration one time password is : {{$otp_code}}</p>
				</div>
				<footer>
					
				</footer>
			</div>
				

		</div>
			


</body>
</html>