<!DOCTYPE html>
<html lang="en" ng-app="Paynow" ng-controller="PaynowController">
<head>
  <title>PayNow</title>
  <base href="/">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('css/google_loader.css')}}">

  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/1.0.3/angular-ui-router.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.min.js"></script>

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
   /* footer {
      background-color: #f2f2f2;
      padding: 25px;
    }*/
    .nav>li>a:focus, .nav>li>a:hover {
        /*text-decoration: none;*/
         background-color: #fff; 
    }
    .nav>li>a{
      padding:10px 0;
    }
    .container-fluid {
      /* padding-right: 15px; */
      /* padding-left: 15px; */
      padding: 15px;
      margin-right: auto;
      margin-left: auto;
    }
    .navbar-blue{
      background: #012b72
    }
    .navbar-inverse {
        border-color: #012b72;
    }
    .navbar-inverse .navbar-nav>li>a {
        color: #f1f1f1;
        font-size: 18px;
    }
    .button-right li{
      border-right: solid 2px rgba(241, 241, 241, 0.33);
    }
    .navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover {
        background-color: rgba(8, 8, 8, 0.16);
    }
  
    .navbar-nav>li {
        width: 115px;
    }
    .container-white{
      background-color: #ffffff;
      top:50%;
      position: relative;
    }
    .row-blue, .menu-container{
      background: #012b72
    }
   /* .menu-container > div> i{
      font-size: 18px;
    }*/
    .menu-container > div : hover{
      border-bottom:solid 2px white;
    }
    /*.menu-container > div> i, .menu-container > div> label{
      color:white;
      padding-left: 15px;
      padding-top: 10px;
    }*/
    .menu-container{
      padding:30px 30px;
      min-height: 200px;
      padding-left: 0px;
    padding-right: 0px;
    }
    .offer-block{
        position: relative;
        top: -177px;
        background: 0999999;
    }
    .offer-white{
      background: white;
      min-height: 280px;
      border-radius: 2px;
      border:solid 2px black; 
    }

    .tab-item 
    {
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
        color: white;
        cursor: pointer;
    }

    /*.tab-item img{
      padding:10px;
    }
*/
  

    .tab-item > label
    {
      font-weight: initial;
      opacity: 0.7;
    }

    .tab-item-active > label {
        border-bottom: 3px solid #00b9f5;
        opacity: 1;
        padding-bottom: 7px;
    }

    .tab-item:hover label 
    {
      opacity: 1;

    }

    .tab-item > i
    {
      font-size: 31px;
      margin-bottom: 7px;
    }

    .tab-content-container
    {
      background: #f3f7f8;
    }

    .tab-content-container > .container
    {
      min-height: 183px;
      background: white;
      top: -105px;
      position: relative;
      border-radius: 3px;
    }
    .container-space{
      margin-top:20px;
      border-radius: 3px;
    }

    .custom-row
    {
      margin-left: 0px;
      margin-right :0px;
    }
    body::-webkit-scrollbar { width: 6px; } 
    body::-webkit-scrollbar-track { -webkit-box-shadow: inset 0 0 6px white; } 
    body::-webkit-scrollbar-thumb { background-color: #00b9f5;; outline: 1px solid #00b9f5;; }

    .top-bar div>i{
      font-size: 22px;
      padding: 10px;
    }
    .top-bar-text{
      display: inline-flex;
    }
    .user-name label{
      padding-top: 10px;
    }
    .pay_acc input, .pay_acc textarea, .payment input, .wallet input{
      margin-top: 20px;
      padding:10px;
      border:0px;
      border-bottom: solid 1px #00b9f5;
      outline: none;
    }
    select{
      width: 100%;
      margin-top: 23px;
      padding:10px 0;
      border:0px;
      border-bottom: solid 1px #999;
    }
    .transfer-btn {
        padding: 15px;
        font-size: 20px;
    }
    .btn-primary {
        color: #fff;
        background-color: #00b9f5;
        border-color: #00b9f5;
        margin-top: 10px;
    }
   /*.pay_acc input[type=tel], .pay_acc input[type=text], .pay_acc select {
        border-radius: 0;
        -moz-border-radius: 0;
        -webkit-border-radius: 0;
        font-size: 15px;
        font-weight: 600;
        padding: 10px 10px 10px 5px;
        border-bottom: 1px solid #ebebeb!important;
        border: none;
        box-sizing: border-box;
        margin-top: 22px;
    }*/
    #country_code:focus{
     
    }
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }
    .modal-input{
      padding: 15px 5px;
      border: 0;
      border-bottom: solid 1px #ddd;
      margin-top: 10px;
    }
    .modal-input:focus{
      padding: 15px 0;
      border: 0;
      border-bottom: solid 1px #ddd;
      margin-top: 10px;
      outline:none;
    }
    .modal-login{
      padding: 0 70px;
    }
    .modal-login button {
        margin-top: 30px;
        margin-bottom: 10px;
        padding: 12px;
        width: 100%;
        font-weight: bold;
        font-size: 14px;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
      border:0px;
      border-bottom-color: none;
      border-bottom: 0px;
      color: #00b9f5;
    }
    .nav-tabs>li>a {
        /*margin:10px 10px 10px;*/
        margin:0px 10px 0px;
        font-size: 18px;
        color: #999;
    }
    .nav-tabs>li>a:active{
      color: #00b9f5;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        border: 0px;
        border-bottom-color: none;  
        border-bottom: 0px;
        border-bottom: solid 2px #00b9f5;
    }
    .nav-tabs>li>a:hover {
        border: 0px;
    }
    .nav-tabs{
      border-bottom: none;
    }
    .nav-tabs>li>a:hover {
        border-color: none !important; 
    }
    .nav-tabs>li>a:hover {
        border-color: #eee #eee #ddd;
    }
    @media (min-width: 768px){
      .modal-dialog {
          /*width: 900px;*/
          width:815px;
    }
    .modal-content{
      border-radius: 2px;
    }
    .bg-blue{
      background : #00b9f5; 
    }
    ul.left img {
        margin: 0px 0px 20px 42px;
    }
    #wallet-container-new ul.left {
        text-align: left;
        background: #00b9f5 !important;
        float: left;
        padding: 138px 42px;
        color: white;
        /*width: 220px;
        height: 237px;*/
    }

    .mod-btn{

    }
    .close {
        color: #1ee0d1;
        opacity:1;
        padding: 10px;
    }
    .modal.in .modal-dialog {
      margin-top: 10.5pc;
    }


    @media (min-width: 1200px) {
        .container {
            width: calc(100% - 100px);
        }
    }


    .header-user-dropdown:hover .user-name + .dropdown-menu
    {
        display: block;
    }

      .modal.fade .modal-dialog {
         transform: scale(0);
         opacity: 0;
         -webkit-transition: all .25s linear;
         -o-transition: all .25s linear;
         transition: all .25s linear;
        }

        .modal.fade.in .modal-dialog {
         opacity: 1;
         transform: scale(1);
        }

        body{
    padding-right: 0px !important;
  }

    body.modal-open
    {
        margin-right: 0px !important;
        overflow: auto;
    }
    .model{
        overflow-y: auto;
    }

  </style>
</head>
<body>

<ui-view></ui-view>

<script type="text/javascript">
  
var paynowApp = angular.module('Paynow', ["ui.router", 'ngStorage']);




paynowApp.service('Auth', function($localStorage) {

    this.isAuthenticated = function() {
        return !!$localStorage.auth;
    }

    this.saveAuth = function(data)
    {
        $localStorage.auth = data;
    }

    this.logout = function()
    {
        $localStorage.auth = null;
    }


    this.getAuthData = function()
    {
        return this.isAuthenticated() ? $localStorage.auth : null;
    }

    this.getUserId = function()
    {
        return this.isAuthenticated() ? this.getAuthData().user.user_id : 0;
    }

    this.getAccessToken = function()
    {
        return this.isAuthenticated() ? this.getAuthData().access_token : "";
    }

});



paynowApp.service('UnauthoriseAccessCheck', ['Auth', '$state', '$rootScope', function(Auth, $state, $rootScope){


    this.check = function(response) 
    {
        if(response && !response.data.success && response.data.type == 'UNAUTHORIZED_ACCESS') {
            console.log('UNAUTHORIZED_ACCESS');
            $state.go('home.pay');
            $rootScope.auth.auth = null;
            Auth.logout();

           return true; 
        } 

        return false;
    }

    
}]);

paynowApp.service('Toaster', [ function(){

    this.callToaster = function(html){

        console.log('TOSTER CALLED');
        var elem = document.createElement('div')
        elem.innerHTML = html;
        elem.className = 'snackbar show'
        
        document.body.appendChild(elem)
       
        setTimeout(function(){ 
            document.body.removeChild(elem)
        }, 3000);

    }
    

}]);

paynowApp.service('MenuView',[ function(){

    this.addActiveClass = function(menu){
      console.log('Menu Called : ', menu);
      $(".tab-item").removeClass('tab-item-active');
      $("#"+menu).addClass('tab-item-active');  
    }

}]);






paynowApp.apis = {};
paynowApp.apis.login = '/api/user/login';
paynowApp.apis.register = '/api/user/register';
paynowApp.apis.verify_mobile = '/api/user/register/otp/send';
paynowApp.apis.verify_otp = '/api/user/register/otp/verify';
paynowApp.apis.send_otp = '/api/user/password/otp/send';  //FOR FORGOT PASSWORD
paynowApp.apis.change_password = 'api/user/password/change'; //FOR CHANGE PASSWOR
paynowApp.apis.get_balance = '/api/user/balance';
paynowApp.apis.get_transaction = '/api/user/transactions'
paynowApp.apis.transferBalance = '/api/user/balance/transfer';
paynowApp.apis.add_balance = '/api/user/stripe/balance/add';
paynowApp.apis.stripe_public_key = "{{$allSettings['stripe_api_public_key']}}";
paynowApp.apis.paypal_client_id = "{{$allSettings['paypal_client_id']}}";
paynowApp.apis.is_paypal_live = {{$allSettings['paypal_live_mode'] == 'true' ? 'true' : 'false'}};
paynowApp.apis.paypal_add_balance = '/api/user/paypal/balance/add';

paynowApp.apis.senangpay_details = '/api/user/senangpay/details';
paynowApp.apis.senangpay_init = '/api/user/senangpay/init';
paynowApp.apis.senangpay_transfer = 'https://app.senangpay.my/payment/submit_payment';    //'https://app.senangpay.my/payment/';
paynowApp.apis.senangpay_response = 'api/user/senangpay/response';


paynowApp.apis.profile_update = '/api/user/update';



paynowApp.config(function($stateProvider, $urlRouterProvider, $locationProvider) {


  //$urlRouterProvider.otherwise('/login');

  $stateProvider
  
    .state({
      name  : 'home',
      abstract : true,
      url: '/',
        templateUrl: 'web/templates/main.html',
        controller : 'PaynowController',
    })
    .state({
      name  : 'home.pay',
      url: '',
        templateUrl: 'web/templates/pay.html',
        controller : 'PayController',
    })
    .state({
        name  : 'home.logout',
        url: '^/logout',
        controller : function(Auth){
            Auth.logout();
        }        
    })
    .state({
      name: 'home.userprofile',
      url: '^/profile',
        templateUrl: 'web/templates/userupdate.html',
        controller : 'ProfileController',
    })

    .state({
      name  : 'home.acceptpayment',
      url: '^/accept/payment',
        templateUrl: 'web/templates/acceptpayment.html',
        controller: 'AcceptPaymentController'
    })
    .state({
        name  : 'home.passbook',
        url: '^/passbook',
        templateUrl: 'web/templates/passbook.html',
        controller : 'PassbookController'
    })
    .state({
        name : 'home.add_money',
        url: '^/add-money',
        templateUrl: 'web/templates/add_money.html',
        controller: 'AddMoneyController'
    })

    .state({
        name : 'home.add_money.stripe',
        url: '^/add-money/stripe',
        templateUrl: 'web/templates/stripe.html',
        controller : 'StripeController'
    })

    .state({
        name : 'home.add_money.paypal',
        url: '^/add-money/paypal',
        templateUrl: 'web/templates/paypal.html',
        controller : 'PaypalController'
    })

    .state({
        name : 'home.add_money.senang',
        url: '^/add-money/senang',
        templateUrl: 'web/templates/senang.html',
        controller : 'SenangpayController'
    })

    .state({
      name  : '404error',
      url: '/404Error',
        templateUrl: 'web/templates/404.html'
    });

    $urlRouterProvider.otherwise('/404Error');

    $locationProvider.html5Mode(true);
    
});


paynowApp.run(['$rootScope', 'Auth', function($rootScope, Auth){

    console.log('AUTH', Auth.getAuthData());
    $rootScope.auth = { auth : Auth.getAuthData() };

}]);



paynowApp.controller('ProfileController', function(
  $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, Toaster){

  //console.log('jkaslkdsajdkl');
    

    $scope.initProfile = function()
    {
        console.log('PROFILE init Called');
        $scope.profile_update_error = {},
        $scope.profile_first_name = '',
        $scope.profile_last_name = '',
        $scope.profile_email = '',
        $scope.profile_country_code = '',
        $scope.profile_mobile_no = '',
        $scope.profile_password = ''
    }
    $scope.initProfile();

    $scope.auth = $rootScope.auth;

    if(!Auth.isAuthenticated()) {
        $state.go('home.pay')
        $scope.$parent.login();
        return;
    }


    $scope.countryCode = function(){

      $.getJSON('web/templates/countrycodes.json', function(data) {   

         $scope.country_code = data;
    
      });        

    }
    $scope.countryCode();

    $scope.userUpdate = function(){

      $http({
            method : 'POST',
            url : paynowApp.apis.profile_update,
            headers: {
                    "Authorization": "Bearer " + $scope.auth.auth.access_token
            },
            data : {
                first_name : $scope.profile_first_name,
                last_name : $scope.profile_last_name,
                country_code : $scope.profile_country_code,
                mobile_no : $scope.profile_mobile_no,
                email : $scope.profile_email
            }
        }).then(function mySuccess(response){

          console.log(response);

            if(!$scope.validateEmail($scope.profile_email)) {
                console.log('invalid email', $scope.profile_email);
                $scope.profile_update_error.email = "Please check your mail id";
                return;
            }


            if(UnauthoriseAccessCheck.check(response)) {
                $scope.$parent.login();
                return;
            }
           
            if(response.data.success) {
                
                console.log('RES',response);
                $scope.logout();
                $scope.login();
                Toaster.callToaster(response.data.text);
                return;
                    
            }else{

                console.log('EER',response);

                if(response.data.data.first_name)
                {
                    $scope.profile_update_error.first_name = response.data.data.first_name;  // FOR FIRST NAME
                }

                if(response.data.data.last_name)
                {
                    $scope.profile_update_error.last_name = response.data.data.last_name; // FOR LAST NAME
                }

                if(response.data.data.email)
                {
                    $scope.profile_update_error.email = response.data.data.email;         // FOR EMAIL
                }

                if(response.data.data.mobile_no)
                {
                    $scope.profile_update_error.mobile = response.data.data.mobile_no;    //   FOR MOBILE 
                }

                if(response.data.data.country_code)
                {
                    $scope.profile_update_error.mobile = response.data.data.country_code; //   FOR COUNTRY CODE
                }
              
            }  


        }, function myError(error){ 

          Toaster.callToaster(response.data.text);
          console.log('Error',error); 

        });
      
    } 

    $scope.changePassword = function()
    {
        $scope.auth = $rootScope.auth;

        if(!Auth.isAuthenticated()) {
            $state.go('home.pay')
            $scope.$parent.login();
            return;
        }

        $http({
            method : 'POST',
            url : paynowApp.apis.profile_update,
            headers: {
                    "Authorization": "Bearer " + $scope.auth.auth.access_token
            },
            data : {
                password : $scope.profile_password
            }
        }).then(function mySuccess(response){

            console.log('PS',response);
            if(response.data.success){
                Toaster.callToaster(response.data.text);
                $scope.logout();
                $scope.login();
                return
            }
            else
            {
                if(response.data.data.password)
                {
                    $scope.profile_update_error.password = response.data.data.password;
                }
            }
         
        }, function myError(error){ 
         
            console.log('PE',response);

        });
    }

});



paynowApp.controller('PaypalController', function(
    $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, Toaster){

    $scope.paypal_spinner = '';
    $scope.auth = $rootScope.auth;

    if(!Auth.isAuthenticated()) {
        $state.go('home.pay')
        $scope.$parent.login();
        return;
    }

    

    $scope.paypalPayBtn = null;
    $scope.paypalSetup = function()
    {

        //document.getElementById('paypal-button-container').innerHTML = '';
        //$scope.pay_with_paypal_btn = false;

        $rootScope.loadScript('paypal.js', 'https://www.paypalobjects.com/api/checkout.js', function(){

            if($scope.paypalPayBtn) {
                return;
            }


            $scope.paypalPayBtn = paypal.Button.render({
               
                env: paynowApp.apis.is_paypal_live ? 'production' : 'sandbox',
                   
                client: {
                    sandbox : paynowApp.apis.paypal_client_id,
                    production: paynowApp.apis.paypal_client_id
                },
              
               
                payment: function() {
               
                    // Make a client-side call to the REST api to create the payment
                    
                    console.log('$scope.paypal_amount', $scope.paypal_amount);
                    if(!$scope.paypal_amount){
                        $scope.paypal_spinner = '';
                        console.log('no amount : ', !$scope.paypal_amount)
                        $scope.paypal_amount_error = 'Please enter the amount and try again';
                        $scope.$apply();
                        return; 
                    }

                    $scope.paypal_spinner = 'spin';
                    console.log('Spinner called');



                    return paypal.rest.payment.create(this.props.env, this.props.client, {
                        transactions: [
                            {
                                amount: { total: $scope.paypal_amount, currency: 'USD' }
                            }
                        ]
                    });
                },
               
                onAuthorize: function(data, actions) {
                        
                    console.log(data, actions)

                    return actions.payment.execute().then(function(payment) {
                        console.log(payment.id);



                        if(!payment.hasOwnProperty('id')) {
                            $scope.paypal_amount_error = 'Some error occured. Try after sometime.'
                            return;
                        }

                        $http({
                            method : 'POST',
                            url : paynowApp.apis.paypal_add_balance,
                            headers: {
                                    "Authorization": "Bearer " + $scope.auth.auth.access_token
                            },
                            data : {
                                payment_id : payment.id,
                                amount : $scope.paypal_amount
                            }
                
                        }).then(function mySuccess(response){

                            console.log(response);

                            if(UnauthoriseAccessCheck.check(response)) {
                                $scope.$parent.login();
                                return;
                            }
                           
                            if(response.data.success) {
                                
                               $scope.auth.auth.user.balance = response.data.data.balance; 
                               $scope.paypal_amount = '';

                               Toaster.callToaster(response.data.text);
                                    
                            }  
                            $scope.paypal_spinner = '';

                        }, function myError(error){ })                        

                    });
            
                }
               
            }, '#paypal-button-container');

            console.log('btn', $scope.paypalPayBtn);

        });



    }

    $scope.paypalSetup();





});




paynowApp.controller('StripeController', function(
    $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, Toaster){

    $scope.auth = $rootScope.auth;

    if(!Auth.isAuthenticated()) {
        $state.go('home.pay')
        $scope.$parent.login();
        return;
    }

    $scope.stripeKey = paynowApp.apis.stripe_public_key;
    $scope.stripe = null;
    $scope.card = null;

    $rootScope.loadScript('stripe-js', 'https://js.stripe.com/v3/', function(){

        var cardElem = document.getElementById('card-element');
        if(!cardElem) {
            return;
        }

        $scope.stripe = Stripe($scope.stripeKey);
        var elements = $scope.stripe.elements();
        $scope.card = elements.create('card', {
            style: {
                base: {
                    iconColor: '#666EE8',
                    color: '#31325F',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '15px',
                    '::placeholder': {
                        color: '#CFD7E0',
                    },
                },
            }
        });

        cardElem.innerHTML = '';
        $scope.card.mount('#card-element');
        $scope.card.on('change', function(event) 
        {
            $scope.setOutcome(event);
        });

      
    });

    $scope.initStripeForm =function()
    {
      $scope.amount = '';
      $scope.card.clear();
    }

    
    $scope.stripeSubmit = function()
    {
        $scope.amount_error = '';

        if($scope.amount == undefined || $scope.amount == null) {
            $scope.amount_error = 'amount_error'
            return;
        }

        $scope.stripe.createToken($scope.card).then($scope.setOutcome);
    }



    $scope.setOutcome = function(result)
    {   
        console.log(result)

        if(result.error) {
            // alert(result.error.message);
            Toaster.callToaster(result.error.message);
            return;
        }

        if(result.token) {

            $http({
                method : 'POST',
                url : paynowApp.apis.add_balance,
                headers: {
                        "Authorization": "Bearer " + $scope.auth.auth.access_token
                },
                data : {
                    charge_token : result.token.id,
                    amount : $scope.amount
                }
                
            }).then(function mySuccess(response){

                console.log(response);

                if(UnauthoriseAccessCheck.check(response)) {
                    $scope.$parent.login();
                    return;
                }
               
                if(response.data.success) {
                    
                    $scope.auth.auth.user.balance = response.data.data.balance; 
                    $scope.initStripeForm();

                    Toaster.callToaster(response.data.text);
                        
                }else{
                    Toaster.callToaster(response.data.text);
                }

            }, function myError(error){

                console.log('Error block Stripe2');

             });

        }


    }



});



paynowApp.controller('SenangpayController', function(
  $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, MenuView){

    $scope.auth = $rootScope.auth;


    console.log('Auths', $scope.auth.auth.user);

    if(!Auth.isAuthenticated()) {
        $state.go('home.pay')
        $scope.$parent.login();
        return;
    }


    $scope.senangpay = function(){


      $http({

        method : 'POST',
        url : paynowApp.apis.senangpay_init,   //REDIRECT : https://app.senangpay.my/payment/
        headers : {
                "Authorization": "Bearer " + $scope.auth.auth.access_token
        },
        data: {
            amount: $scope.senang_amount
        }


      }).then(function mySuccess(response){

          console.log('RESP',response);

          // if(UnauthoriseAccessCheck.check(response)) {
          //     // $scope.$parent.login();
          //     // return;
          //     console.log('unauthorized access');
          // }
         
          if(response.data.success) {

            // window.location('https://app.senangpay.my/payment/'+$scope.merchantKey+'/');
            console.log('RESp2 :', response);
            console.log('URL1', response.data.data.url);

             console.log('URL2', response.data.data.url+'?detail='+response.data.data.params.detail+'&amount='+response.data.data.params.amount+'&order_id='+response.data.data.params.order_id+'&name='+response.data.data.params.name+'&email='+response.data.data.params.email+'&phone='+response.data.data.params.phone+'&hash='+response.data.data.params.hash);
             // window.location = response.data.data.url;

             $scope.postToSenangpay = function() {
                var form = document.createElement("form");
                var element1 = document.createElement("input"); 
                var element2 = document.createElement("input");
                var element3 = document.createElement("input");  
                var element4 = document.createElement("input");
                var element5 = document.createElement("input");
                var element6 = document.createElement("input");
                var element7 = document.createElement("input");


                form.method = "POST";
                form.name = "order";
                form.action = response.data.data.url;  


                element1.value=response.data.data.params.detail;
                element1.name="detail";
                form.appendChild(element1);  

                element2.value=response.data.data.params.amount;
                element2.name="amount";
                form.appendChild(element2);

                element3.value=response.data.data.params.order_id;
                element3.name="order_id";
                form.appendChild(element3);

                element4.value=response.data.data.params.name;
                element4.name="name";
                form.appendChild(element4);

                element5.value=response.data.data.params.email;
                element5.name="email";
                form.appendChild(element5);

                element6.value=response.data.data.params.phone;
                element6.name="phone";
                form.appendChild(element6);

                element7.value=response.data.data.params.hash;
                element7.name="hash";
                form.appendChild(element7);

                document.body.appendChild(form);


                 form.submit();
            }
            $scope.postToSenangpay();
                  
          }else{
              
            console.log('response error block');

          }

      }, function myError(error){

          console.log('Error block SenangPay');

      });

    }

    // $scope.SenangPayResponse = window.location = paynowApp.apis.senangpay_response;

    // console.log('RSP : ', $scope.SenangPayResponse);


});





paynowApp.controller('AddMoneyController', function(
    $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, MenuView){

    MenuView.addActiveClass('add-money');

    $scope.auth = $rootScope.auth;

    if(!Auth.isAuthenticated()) {
        $state.go('home.pay')
        $scope.$parent.login();
        return;
    }

    $scope.paypal = function()
    {
        $state.go('home.add_money.paypal');
        return;
    }
    $scope.paypal();


    $scope.stripe = function()
    {
        $state.go('home.add_money.stripe');
        return;
    }

    $scope.senang = function()
    {
        $scope.go('home.add_money.senang');
        return;
    }


});



paynowApp.controller('PassbookController', function(
    $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, MenuView) {

    MenuView.addActiveClass('passbook');

    $scope.auth = $rootScope.auth;

    if(!Auth.isAuthenticated()) {
        $state.go('home.pay')
        $scope.$parent.login();
        return;
    }

    $scope.getBalance = function(){
     
     
        $http({
            method : 'GET',
            url : paynowApp.apis.get_balance,
            headers: {
                    "Authorization": "Bearer " + $scope.auth.auth.access_token
            }
        }).then(function mySuccess(response){

            if(UnauthoriseAccessCheck.check(response)) {
                $scope.$parent.login();
                return;
            }
           
            if(response.data.success) {
                
                $scope.auth.auth.user.balance = response.data.data.balance; 
                    
            }  

        }, function myError(error){ });

    }

    $scope.getBalance();

    $scope.next_trans_url = paynowApp.apis.get_transaction;
    $scope.transactions = [];

    $scope.getTransactions = function()
    {
      $http({

        method : 'GET',
        url : $scope.next_trans_url,
        headers : {
            "Authorization" : "Bearer " + $scope.auth.auth.access_token
        }

      }).then(function mySuccess(response){

        console.log('Result : ',response)

        $scope.transactions = $scope.transactions.concat(response.data.data.transactions);


        //$scope.transactions = response.data.data.transactions;
        console.log('TRANS : ',$scope.transactions);


        if(response.data.data.paging.more_pages) {
            $scope.next_trans_url = response.data.data.paging.next_page_url;
        } else  {
             $scope.next_trans_url = '';
        }



      }, function myError(error) { });
    }

    $scope.getTransactions();


});

// $scope.accept_payment(){
//   $(".tab-item").removeClass('tab-item-active');
//   $("#accept-payment").addClass('tab-item-active');  
// }

paynowApp.controller('AcceptPaymentController', function
    ($state, $rootScope, $scope, $http, $state, Auth, MenuView, Toaster){

        MenuView.addActiveClass('accept-payment');

        // $scope.accept_payment();

        $scope.auth = $rootScope.auth;

        if(!Auth.isAuthenticated()) {
            $state.go('home.pay')
            $scope.$parent.login();
            return;
        }
        

        $rootScope.loadScript('qrcode-lib', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js', function(){

            $("#payment-qrcode").html('');

            jQuery('#payment-qrcode').qrcode({
                 text : $scope.auth.auth.user.mobile_no
            }); 

        });





    $scope.download = function()
    {


        $rootScope.loadScript('html2canvas', 'https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js', function(){

            html2canvas(document.querySelector("#qr-container"), {
                onrendered: function(c) {
         
                    var link = document.getElementById('downloadLink');
                    link.setAttribute('download', 'wallet_payment.png');
                    link.setAttribute('href', c.toDataURL("image/png").replace("image/png", "image/octet-stream"));
                    link.click();
                    Toaster.callToaster('Downloading...')

                }
            });

        }); 


    }




});





paynowApp.controller('PaynowController', function(
    $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck) {

    $scope.auth = $rootScope.auth;
    
    $scope.countryCode = function(){

      $.getJSON('web/templates/countrycodes.json', function(data) {   

         $scope.country_code = data;
    
      });        

    }
    $scope.countryCode();    

    $rootScope.loadScript = function(id, url, callback) 
    {

        if(document.querySelectorAll('[id="'+id+'"]').length > 0){
            callback();
            return;
        }

        var script = document.createElement( "script" )
        script.type = "text/javascript";
        script.id = id;

        if(script.readyState) {  //IE
        
            script.onreadystatechange = function() {
                if ( script.readyState === "loaded" || script.readyState === "complete" ) {
                    
                    script.onreadystatechange = null;
                    callback();
                }   
            };
    
        } else {  //Others
        
            script.onload = function() {
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName( "head" )[0].appendChild( script );
    }







    $scope.go = function(state)
    {
        $state.go(state);
    }

    $scope.login = function(){
        $('#myModal').modal('show');
        $scope.initLogin();
        $scope.initRegister();
    }

    $scope.logout = function(){
        Auth.logout();
        $scope.auth.auth = null;
        console.log('logout');
        $scope.go('home.pay')
    }

    $scope.initLogin = function()
    {
        console.log('LOGIN init called');
        $scope.user_login_error = {};
        $scope.loginShow = 1; // LOGIN CALLS
        $scope.login_user_otp = '';
        $scope.login_user_mobile = '';
        $scope.login_user_password = '';
    }

    $scope.initRegister = function()
    {
        console.log('REGISTER init called');
        $scope.user_register_error = {};
        $scope.register_user_mobile_no = '';
        $scope.register_user_password = '';
        $scope.register_user_fname = '';
        $scope.register_user_lname = '';
        $scope.register_user_email = '';

    }

    

    $scope.initForgot = function()
    {
        $scope.user_country_code = '';
        $scope.user_mobile_no = '';
    }

    $scope.forgotPassword = function()
    {
        $scope.initForgot();
        $scope.loginShow = 3 // SENT OTP CALLS
    }

    $scope.send_otp_spinner = '';
    $scope.sendOTP = function()
    {
        console.log('called send OPT');

        $scope.countryCode = function(){

          $.getJSON('web/templates/countrycodes.json', function(data) {   

             $scope.country_code = data;
        
          });        

        }
        $scope.countryCode();

        $scope.forgot_login_error = {}

        $scope.send_otp_spinner = 'spin';

        $http({
            method : 'POST',
            url : paynowApp.apis.send_otp,
            headers : {

            },  
            data : {
                country_code : $scope.user_country_code,
                mobile_no : $scope.user_mobile_no
            }
        }).then(function mySuccess(response){
                console.log('success', response);
            if(response.data.success) {

                $scope.loginShow = 4;  //CHANGE PASSWORD CALLS
         
            } else {

                if(response.data.type == 'MOBILE_NOT_REGISTERED')
                {
                    $scope.forgot_login_error.mobile = response.data.text;
                }
         

            }

            $scope.send_otp_spinner = '';

        }, function myError(error){ 
              
              console.log('Failed'); 
              $scope.send_otp_spinner = '';
        });

    }

  $scope.change_pw_spinner = '';
  $scope.changePassword = function()
  {
    console.log('change password block');
    $scope.change_password_error = {};

    $scope.change_pw_spinner = 'spin';

    $http({
        method : 'POST',
        url : paynowApp.apis.change_password,
        headers : {

        },
        data : {
            country_code : $scope.user_country_code,
            mobile_no : $scope.user_mobile_no,
            new_password : $scope.new_password,
            otp_code : $scope.otp_code
        }
    }).then(function mySuccess(response){
        console.log('success', response);
        

        if(response.data.success) {  

            $scope.initLogin();
            $scope.loginShow = 1;

        }  else {
            
            if(response.data.data.otp_code){
                $scope.change_password_error.otp_code = 'OTP verify failed. Enter correct OTP';
            }
            if(response.data.data.new_password){
                $scope.change_password_error.new_password = response.data.data.new_password;
            }
            
        }

        $scope.change_pw_spinner = '';

    }, function myError(error){
        console.log('ERROR !!!!!');
        $scope.change_pw_spinner = '';
    })
  }


  $scope.verify_otp = '';
  $scope.verifyOTP = function()
  {
        $scope.user_login_error = {};

        $scope.verify_otp = 'spin';

        if(!$scope.login_user_otp) {
            $scope.verify_otp = '';
            $scope.user_login_error.verify_otp = 'Enter OTP code'
            return;
        }

        $http({
            method : 'POST',
            url : paynowApp.apis.verify_otp,
            headers: {
                    "Authorization": "Bearer " + $scope.tempAuth.access_token
            },
            data : {
                otp_code : $scope.login_user_otp
            }
        }).then(function mySuccess(response){

            if(UnauthoriseAccessCheck.check(response)) {
                $scope.$parent.login();
                return;
            }

            console.log('verify otp response', response.data); 
            if(response.data.success) {
                    
                    //login success save auth user hrer close login modal
                    Auth.saveAuth($scope.tempAuth);
                    $rootScope.auth.auth = $scope.tempAuth;
                    $scope.auth = $rootScope.auth;
                    $('#myModal').modal('hide');
            }   
            else 
            {
                $scope.user_login_error.verify_otp = 'OTP verify failed. Enter correct OTP'
            }

            $scope.verify_otp = '';

        }, function myError(error){

            $scope.verify_otp = '';

        });

  }



  $scope.resendOTP = function()
  {
        $scope.sendVerifyOTP($scope.tempAuth.access_token, function(response){
            alert('otp sent');
        });
  }


  $scope.send_verify_otp_spinner = '';
  $scope.sendVerifyOTP = function(access_token, callback = '', errorCallback = null)
  {
        if(!access_token) {
            console.log('otp send response', 'no access token');
            return;
        }

        $scope.send_verify_otp_spinner = 'spin';

        $http({
            method : 'POST',
            url : paynowApp.apis.verify_mobile,
            headers: {
                    "Authorization": "Bearer " + access_token
             }
        }).then(function mySuccess(response){

            if(UnauthoriseAccessCheck.check(response)) {
                $scope.$parent.login();
                return;
            }

            console.log('otp send resonse', response.data);

            if(callback == '') {
                callback(response);    
            }
            
            $scope.send_verify_otp_spinner = '';

        }, function myError(error){

            $scope.send_verify_otp_spinner = '';

            if(errorCallback) {
                errorCallback(error);
            }

        });
  }


  $scope.tempAuth = null;
  $scope.login_spinner = '';
  $scope.doLogin = function()
  {


        $scope.user_login_error = {};
        
        $scope.login_spinner = 'spin';

        $http({
            method : "POST",
            url : paynowApp.apis.login,
            data : {
                mobile_no : $scope.login_user_mobile,
                password : $scope.login_user_password
            }
        }).then(function mySuccess(response) {

            if(response.data.success) {

                $scope.tempAuth = response.data.data;
                $scope.loginShow = 2;


                $scope.sendVerifyOTP($scope.tempAuth.access_token, function(response){});
               
            } else {

                if(response.data.data.mobile_no) {
                    $scope.user_login_error.mobile = response.data.data.mobile_no;
                }

                 if(response.data.data.password) {
                    $scope.user_login_error.password = response.data.data.password;
                }


            }

            $scope.login_spinner = '';

            console.log(response.data);
            
        }, function myError(response) {
        
            /*$scope.error = response.data*/
            console.log('error', response);
             $scope.login_spinner = '';

        });
  }



    $scope.validateEmail = function(email) 
    {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    $scope.register_spinner = '';
    $scope.doRegister =function()
    {


        $scope.user_register_error = {};
      
        if(!$scope.validateEmail($scope.register_user_email)) {
            console.log('invalid email', $scope.register_user_email);
            $scope.user_register_error.email = "Please check your mail id";
            return;
        }

        $scope.register_spinner = 'spin';

        $http({
            method : "POST",
            url : paynowApp.apis.register,
            data : {

                first_name : $scope.register_user_fname,
                last_name : $scope.register_user_name,
                email : $scope.register_user_email,
                country_code : $scope.register_user_country_code,
                mobile_no : $scope.register_user_mobile_no,
                password : $scope.register_user_password
            }
        }).then(function mySuccess(response) {

            if(response.data.success) {
                console.log('success reg');
                 $(".login-container").find('a[href="#login"]').tab('show');
            } 
            else 
            {
                console.log('reg error',response);
                if(response.data.data.first_name) {
                    $scope.user_register_error.first_name = response.data.data.first_name;  // FOR FIRST NAME
                }

                if(response.data.data.last_name) {
                    $scope.user_register_error.last_name = response.data.data.last_name; // FOR LAST NAME
                }

                if(response.data.data.email) {
                    $scope.user_register_error.email = response.data.data.email;         // FOR EMAIL
                }

                if(response.data.data.mobile_no) {
                    $scope.user_register_error.mobile = response.data.data.mobile_no;    //   FOR MOBILE 
                }

                if(response.data.data.country_code){
                    $scope.user_register_error.mobile = response.data.data.country_code; //   FOR COUNTRY CODE
                }

                if(response.data.data.password) {
                    $scope.user_register_error.password = response.data.data.password;   //FOR PASSSWORD
                }
            }

            $scope.register_spinner = '';

            console.log(response.data);
            
        }, function myError(response) {
        
            /*$scope.error = response.data*/
            console.log('error', response);

            $scope.register_spinner = '';

        });
    }
});

paynowApp.controller('PayController', function(
    $state, $rootScope, $scope, $http, $state, Auth, UnauthoriseAccessCheck, MenuView, Toaster) {

    console.log('pay controller called');
    MenuView.addActiveClass('pay');

    $scope.auth = $rootScope.auth;

    
    $scope.countryCode = function(){

      $.getJSON('web/templates/countrycodes.json', function(data) {   

         $scope.country_code = data;
    
      });        

    }

    $scope.countryCode();


    $scope.scanner = null;
    $scope.openQRScannerModal = function()
    {
        $("#qr-scanner-modal").modal('show');

        $rootScope.loadScript('qr-scanner', 'https://rawgit.com/schmich/instascan-builds/master/instascan.min.js', 
        function(){

            if(!$scope.scanner) {
                $scope.scanner = new Instascan.Scanner({ video: document.getElementById('qr-scanner-preview') });
            }
            

            $scope.scanner.addListener('scan', function (content) {
                console.log(content);
                $scope.pay_user_receiver_mobile = parseInt(content);
                $scope.$apply();
                $scope.closeQRScannerModal();
            });

            Instascan.Camera.getCameras().then(function (cameras) {

                if (cameras.length > 0) {
                  $scope.scanner.start(cameras[0]);
                } else {
                  console.error('No cameras found.');
                }

              }).catch(function (e) {
                
                console.log(e);
                $("#qr-scanner-modal").modal('hide');
                Toaster.callToaster('Error! some intenal error, try after sometimes');

              });

        });

         
    }


    $scope.closeQRScannerModal = function()
    {
        if($scope.scanner) {
            $scope.scanner.stop();
        }

        $("#qr-scanner-modal").modal('hide');
    }







    $scope.initPaymentForm = function()
    {
        $scope.pay_user_receiver_mobile = '';
        $scope.pay_user_amount = '';
        $scope.pay_user_remark = '';
        $scope.message = '';
        $scope.message_class = '';
    }

    $scope.initPaymentForm();

    $scope.closeSuccessPaymentModal = function()
    {
        $("#paymentSuccessModal").modal('hide');
        $scope.initPaymentForm();
    }

    // $scope.toaster = function(html){

    //     var elem = document.createElement('div')
    //     elem.innerHTML = html;
    //     elem.className = 'snackbar show'
        
    //     document.body.appendChild(elem)
       
    //     setTimeout(function(){ 
    //         document.body.removeChild(elem)
    //     }, 3000);
    // }

    $scope.transfer_spinner = '';
    $scope.transferBalance = function(){

        if(!Auth.isAuthenticated()) {
            $state.go('home.pay')
            $scope.$parent.login();
            return;
        }

        $scope.transfer_spinner = 'spin';
     
        $scope.message_class = '';
     
        $http({
            method : 'POST',
            url : paynowApp.apis.transferBalance,
            headers: {
                    "Authorization": "Bearer " + $scope.auth.auth.access_token
            },
            data : {
                country_code : $scope.pay_user_country_code,
                receiver_mobile : $scope.pay_user_receiver_mobile,
                amount : $scope.pay_user_amount,
                remarks : $scope.pay_user_remark
            }
        }).then(function mySuccess(response){

            if(UnauthoriseAccessCheck.check(response)) {
                $scope.$parent.login();
                return;
            }


           // console.log(response.data);
            if(response.data.success) {
                
                console.log(response);
                $scope.auth.auth.user.balance = response.data.data.balance.toFixed(2).toString();
                $scope.message = response.data.text;
                $scope.balance = response.data.data.balance;
                
                $("#paymentSuccessModal").modal('show');

                Toaster.callToaster(response.data.text);  // ***** TOASTER CALLED *****
            } 
            else{
                console.log(response.data.text);
                if(response.data.data.country_code){
                    $scope.message = response.data.data.country_code;
                }

                else if(response.data.data.receiver_mobile){
                    $scope.message = response.data.data.receiver_mobile;

                }
                else if(response.data.type == 'SAME_NUMBER'){
                    $scope.message = response.data.text;   
                }
                else if(response.data.data.amount){
                    $scope.message = response.data.data.amount;
                }else if(response.data.type == 'LOW_BALANCE'){
                    $scope.message = response.data.text;
                }
                $scope.text = response.data.text;
                console.log(response);
                $scope.message_class = 'alert-danger';
                /*$('.alert').show();
                $('.alert').addClass('alert-danger');
                $('.alert').removeClass('alert-success');*/
            }

            $scope.transfer_spinner = '';


        }, function myError(error){ 

              $scope.transfer_spinner = '';

        });

    }

    // $scope.getBalance();

});




</script>

</body>
</html>
